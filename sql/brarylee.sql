CREATE DATABASE  IF NOT EXISTS `brarylee` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `brarylee`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: brarylee
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `author` varchar(45) NOT NULL,
  `publisher` varchar(45) NOT NULL,
  `idgenre` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `edition` varchar(45) DEFAULT NULL,
  `isbn` varchar(45) NOT NULL,
  `note` varchar(45) DEFAULT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1,'Alice nel Paese delle Meraviglie','Lewis Carroll','De Bastiani',2,2018,'Seconda','978-8884665218','testing',1),(2,'Napoli Nella Storia','Attilio Wanderlingh','Intra Moenia',2,2016,'Seconda','978-8874211906',NULL,1),(3,'Autopsia Virtuale','Patricia Cornwell','Mondadori',3,2011,'Prima','978-8804611370',NULL,1),(4,'It','Stephen King','Sperling & Kupfer',4,1990,'Terza','978-8878240810',NULL,1),(5,'Analisi Matematica II. Schede Ed Esercizi','Marina Ghisi - Massimo Gobbino','Esculapio',7,2017,'Prima','978-8893850438',NULL,1),(6,'Grammatica italiana','Maurizio Dardano','Zanichelli',6,2002,'Terza','978-8808093844',NULL,1);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `copies`
--

DROP TABLE IF EXISTS `copies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `idbook` int(11) NOT NULL,
  `idposition` int(11) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `copies`
--

LOCK TABLES `copies` WRITE;
/*!40000 ALTER TABLE `copies` DISABLE KEYS */;
INSERT INTO `copies` VALUES (1,'RO0000101',1,13,1),(2,'ST0000201',2,2,1),(3,'RO0000102',1,1,1),(4,'MA0000101',5,7,1),(5,'MA0000102',5,8,1),(6,'MA0000103',5,9,1),(7,'HO0000101',4,5,1),(8,'TH0000101',3,13,1),(9,'GR0000101',6,12,1),(10,'GR0000102',6,12,1);
/*!40000 ALTER TABLE `copies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
INSERT INTO `genres` VALUES (1,'Romanzo',1),(2,'Storico',1),(3,'Giallo',1),(4,'Horror',1),(5,'Thriller',1),(6,'Grammatica',1),(7,'Matematica',1);
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `handlings`
--

DROP TABLE IF EXISTS `handlings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `handlings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `idcopy` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `expiry` datetime NOT NULL,
  `restitution` datetime DEFAULT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `handlings`
--

LOCK TABLES `handlings` WRITE;
/*!40000 ALTER TABLE `handlings` DISABLE KEYS */;
/*!40000 ALTER TABLE `handlings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `iduser` int(11) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'admin','admin',1,1),(2,'robin','87654321',2,1),(3,'joker','23456789',3,1),(4,'superman','98765432',4,1),(5,'pippobaudo','34567890',5,1),(6,'luigi','123',6,1);
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookcase` varchar(45) NOT NULL,
  `shelf` varchar(45) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions`
--

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` VALUES (1,'A','01',1),(2,'A','02',1),(3,'A','03',1),(4,'A','04',1),(5,'B','01',1),(6,'B','02',1),(7,'C','01',1),(8,'C','02',1),(9,'C','03',1),(10,'D','01',1),(11,'D','02',1),(12,'E','01',1),(13,'F','01',1);
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typologies`
--

DROP TABLE IF EXISTS `typologies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typologies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typologies`
--

LOCK TABLES `typologies` WRITE;
/*!40000 ALTER TABLE `typologies` DISABLE KEYS */;
INSERT INTO `typologies` VALUES (1,'Utente Esterno',1),(2,'Studente',1),(3,'Professore',1),(4,'Bibliotecario',1);
/*!40000 ALTER TABLE `typologies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `birthdate` datetime NOT NULL,
  `birthplace` varchar(45) NOT NULL,
  `cf` varchar(45) NOT NULL,
  `document` varchar(45) NOT NULL,
  `numberdocument` varchar(45) NOT NULL,
  `datedocument` datetime NOT NULL,
  `release` varchar(45) NOT NULL,
  `idtypology` int(11) NOT NULL,
  `level` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `enabled` tinyint(4) NOT NULL,
  `visibility` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Giovanni','d\'Alise','1974-12-15 00:00:00','Casoria (Na)','DLSGNN74T15B990U','Patente','U2D293827H','2005-12-15 00:00:00','MCTC-NA',4,'admin','Via Roma','Casalnuovo (Na)','5556060512','giovanni@email.it',1,1),(2,'Davide','Angelino','1991-06-05 00:00:00','Napoli','NGLDVD91H05F839Q','C.I.','AO3949JX','2009-04-03 00:00:00','Caivano',2,'user','Corso Europa','Caivano (Na)','5556758943','davide.a@email.it',0,1),(3,'Francesco','Angelino','1984-05-05 00:00:00','Napoli','NGLFNC84E05F839U','Patente','U4N223011J','2016-06-15 00:00:00','U.C.O.',1,'user','Corso Garibaldi','Caivano (Na)','5552030291','francesco@email.it',0,1),(4,'Davide ','Buonomo','1976-07-20 00:00:00','Napoli','BNMDVD76L20F839V','C.I.','AX03221K','2010-11-15 00:00:00','Casoria',3,'user','Via Einaudi','Casoria (Na)','5551020391','davide.b@email.it',1,1),(5,'Angelo','Riccio','1977-12-25 00:00:00','Napoli','RCCNGL77T25F839Z','Passaporto','AZ20303LL','2015-07-15 00:00:00','Napoli',3,'user','Via Atripalda','Avellino','5552020112','angelo@email.it',1,1),(6,'luigi','criscuolo','1990-11-18 00:00:00','san giorgio','crslgu90asafsf','carta identità','123456','2018-05-03 00:00:00','napoli',1,'user','via repub','napoli','08123456','luigi@mail.com',1,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-14 15:36:25
