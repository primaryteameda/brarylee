Il sistema permette la gestione di una biblioteca scolastica. La biblioteca � accessibile non solo dal personale scolastico (divisi per ruolo) e dagli alunni iscritti alla scuola, ma anche da utenti esterni.

I libri presenti nella biblioteca hanno i seguenti dati: titolo, autore, genere, editore, anno di pubblicazione, edizione e codice ISBN. 

Gli utilizzatori sono divisi in due gruppi: Amministratore e Utenti. 

L' amministratore pu� aggiungere libri, aggiornando l'elenco presente in biblioteca; visualizzare la lista dei prestiti e delle prenotazioni effettuate dagli utenti. 
Pu� registrare nuovi utenti, abilitare quelli gi� registrati o disabilitare utenti, causa restituzione di uno o pi� libri oltre i tempi previsti. 
L'amministratore pu� effettuare una ricerca degli utenti presenti nell'archivio della biblioteca tramite: cognome, codice fiscale, citt� di residenza e ruolo scolastico.
 

Gli utenti, senza registrarsi, possono visualizzare l'elenco dei libri presenti nella biblioteca, cercare un libro specifico inserendo: autore, titolo, genere o codice ISBN; mentre previa registrazione possono consultare in loco o chiedere in prestito uno o pi� libri, verificando la disponibilit� e la posizione tra gli scaffali della biblioteca. Possono prendere in prestito un libro o prenotarlo per una consultazione successiva.
Gli utenti per registrarsi devono inserire: nome, cognome, luogo e data di nascita, codice fiscale, documento, indirizzo, telefono, mail, e codice cliente;   
L' utente successivamente verr� abilitato dall'amministratore della biblioteca esibendo il proprio documento.
In futuro l'utente avr� la possibilit� di cambiare solo alcuni dati personali: documento, indirizzo, telefono ed email.
