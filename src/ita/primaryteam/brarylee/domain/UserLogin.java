package ita.primaryteam.brarylee.domain;

public class UserLogin {
	
	private static UserLogin instance = null; //STIAMO UTLIZZANDO Design Pattern - Singleton Pattern
	
	private Integer userId;
	
	private UserLogin() {}
	
	public static synchronized UserLogin getInstance() {
		if (instance == null) {
			instance = new UserLogin();
		}
		
		return instance;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
