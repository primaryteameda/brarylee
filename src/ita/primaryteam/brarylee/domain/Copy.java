package ita.primaryteam.brarylee.domain;

//JAVABEAN PER MEMORIZZARE I DATI INERENTI LE COPIE DI UN LIBRO
public class Copy {
	
	// ATTRIBBUTI
	private int id;
	private String code;
	private Position position;
	private boolean visibility;

	// METODI
	public Copy() {

		this.id = 0;
		this.setCode("");
		position = new Position();
		this.setVisibility(true);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public boolean isVisible() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override 
	public String toString() {
		return this.code + " Posizione: " + this.position.getBookcase() + " " + this.position.getShelf();
	}
}
