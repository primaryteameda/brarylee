package ita.primaryteam.brarylee.domain;

//JAVABEAN PER MEMORIZZARE I GENERI DI UN LIBRO
public class Genre {
	
	// ATTRIBBUTI
	private int id;
	private String name;
	private Boolean visibility;
	static final private String FORMAT = "%-5s%-10s";

	// METODI
	public Genre() {
		this.setId(0);
		this.setName("");
		this.setVisibility(true);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean isVisible() {
		return visibility;
	}
	public void setVisibility(Boolean visibility) {
		this.visibility = visibility;
	}
	
	public static void stampaHeader() {
		String str = String.format(FORMAT, "ID", "Genere") ;
		System.out.println(str);
		str = Book.pad(20, '-');
	}
	
	public void stampaGenre() {
		String str = String.format(FORMAT, this.id, this.name);
		System.out.println(str);
	}
	
	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override
	public String toString() {
		return this.id + " " + this.name + " " + this.visibility;
	}
}
