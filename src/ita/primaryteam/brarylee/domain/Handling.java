package ita.primaryteam.brarylee.domain;

import java.sql.Timestamp;

//JAVABEAN PER MEMORIZZARE LE MOVIMENTAZIONI DI UN LIBRO ( PRESTITI, CONSULTAZIONI, PRENOTAZIONI) E MEMORIZZARE LE DATE 
public class Handling {
	
	// ATTRIBBUTI
	private int id;
	private String type;
	private Book book;   // OGGETTO PER RICHIAMARE LA CLASSE "BOOK"
	private User user;   // OGGETTO PER RICHIAMARE LA CLASSE "USER"
	private Copy copy;
	
	private Timestamp start;
	private Timestamp expiry;
	private Timestamp restitution;
	private Boolean visibility;
	static final private String FORMAT = "%-5s%-15s%-15s%-15s%-45s%-20s%-20s%-20s";
	static final private String FORMAT2 = "%-5s%-15s%-15s%-15s%-45s%-20s%-20s%-20s%-20s%-10s%-15s";

	// METODI
	public Handling() {
		this.setId(0);
		this.setType("");
		book = new Book();
		user = new User();
		this.setVisibility(true);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Timestamp getStart() {
		return start;
	}

	public void setStart(Timestamp start) {
		this.start = start;
	}

	public Timestamp getExpiry() {
		return expiry;
	}

	public void setExpiry(Timestamp expiry) {
		this.expiry = expiry;
	}

	public Timestamp getRestitution() {
		return restitution;
	}

	public void setRestitution(Timestamp restitution) {
		this.restitution = restitution;
	}

	public Boolean isVisible() {
		return visibility;
	}

	public void setVisibility(Boolean visibility) {
		this.visibility = visibility;
	}
	
	public Copy getCopy() {
		return copy;
	}

	public void setCopy(Copy copy) {
		this.copy = copy;
	}
	
	public static void stampaHeader() {
		String str = String.format(FORMAT, "ID", "TIPO", "ISBN", "CODICE COPIA", "TITOLO", "INIZIO", "SCADENZA", "RESTITUZIONE") ;
		System.out.println(str);
		str = Book.pad(150, '-');
	}

	public static void stampaHeaderAdmin() {
		String str = String.format(FORMAT2, "ID", "TIPO", "ISBN", "CODICE COPIA", "TITOLO", "INIZIO", "SCADENZA", "RESTITUZIONE", "CODICE FISCALE", "COGNOME", "NOME" ) ;
		System.out.println(str);
		str = Book.pad(200, '-');
	}
	
	private String date() {
		int day = this.start.getDate();
		int year = this.start.getYear()+1900;
		int month=this.start.getMonth()+1;
		int hours = this.start.getHours();
		int minutes= this.start.getMinutes();
		return  day + "/" + month + "/" + year + " " + hours + ":" + this.start.getMinutes();
	}
	
	private String dateE() {
		int dayE =  this.expiry.getDate();
		int yearE = this.expiry.getYear()+1900;
		int monthE= this.expiry.getMonth()+1;
		int hoursE = this.expiry.getHours();
		int minutesE= this.expiry.getMinutes();
		return  dayE + "/" + monthE + "/" + yearE + " " + hoursE + ":" + minutesE;
		
	}
	
	private String dateR() {
		int dayR =  this.restitution.getDate();
		int yearR = this.restitution.getYear()+1900;
		int monthR= this.restitution.getMonth()+1;
		int hoursR = this.restitution.getHours();
		int minutesR= this.restitution.getMinutes();
		return  dayR + "/" + monthR + "/" + yearR + " " + hoursR + ":" + minutesR;
	}
	
	public void stampaHandlingUser() {
		
		String str = String.format(FORMAT, this.id, this.getType(), this.book.getIsbn(), this.copy.getCode(), this.book.getTitle(), date(), dateE(), (this.restitution != null ? dateR() : "")) ;
		System.out.println(str);
		
	}
	
	public void stampaHandlingAdmin() {
		String str = String.format(FORMAT2, this.id, this.getType(), this.book.getIsbn(), this.copy.getCode(), this.book.getTitle(), date(), dateE(), (this.restitution != null ? dateR() : ""), 
				this.user.getCf(), this.user.getSurname(), this.user.getName() ) ;
		System.out.println(str);
		
	}
	
	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override
	public String toString() {
		return "id: " + this.id + " - Tipo: " + this.getType() + " - isbn: " + this.book.getIsbn() + " - codice copia: " + this.copy.getCode() + " - titolo: " + this.book.getTitle() + " - scadenza: " + this.expiry + 
				" - user: " + this.user.getCf() + " - Nome: " + this.user.getName() + " - Cognome: " + this.user.getSurname();
	}
	
	public String toStringUser() {
		return "id: " + this.id + " - Tipo: " + this.getType() + " - isbn: " + this.book.getIsbn() + " - codice copia: " + this.copy.getCode() + " - titolo: " + this.book.getTitle() + " - scadenza: " + this.expiry + " - restituzione: " + this.restitution;
	}
}
