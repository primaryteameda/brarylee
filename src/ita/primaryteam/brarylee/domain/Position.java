package ita.primaryteam.brarylee.domain;

//JAVABEAN PER MEMORIZZARE LA POSIZIONE DEL LIBRO ALL'INTERNO DELLA BIBLIOTECA
public class Position {

	
	// ATTRIBBUTI
	private int id;
	private String bookcase;
	private String shelf;
	private boolean visibility;
	static final private String FORMAT = "%-5s%-10s%-5s";
	
	// METODI
	public Position() {

		this.setId(0);
		this.setBookcase("");
		this.setShelf("");
		this.setVisibility(true);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBookcase() {
		return bookcase;
	}

	public void setBookcase(String bookcase) {
		this.bookcase = bookcase;
	}

	public String getShelf() {
		return shelf;
	}

	public void setShelf(String shelf) {
		this.shelf = shelf;
	}

	public boolean isVisible() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}
	
	public static void stampaHeader() {
		String str = String.format(FORMAT, "ID", "SCAFFALE", "MENSOLA") ;
		System.out.println(str);
		str = Book.pad(30, '-');
	}
	
	public void stampaPosition() {
		String str = String.format(FORMAT, this.id, this.bookcase, this.shelf);
		System.out.println(str);
	}
	
	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override
	public String toString() {
		return this.id + " " + this.bookcase + " " + this.shelf + " " + this.visibility;
	}
}
