package ita.primaryteam.brarylee.domain;

import java.util.Calendar;
import java.util.StringTokenizer;

public class DateTime {   // questo metodo  serve per caMBIARE IL TIPO DI DATA DA STAMPARE

	private  final String SPLITTER_DB_DATE = "-";
	private  final String SPLITTER_IT_DATE = "/";
	private  final String SPLITTER_TIME = ":";

	private int day;
	private int month;
	private int year;
	private int hour;
	private int minute;
	private int second;

	public DateTime() {  

		this.day = 0;
		this.month = 0;
		this.year = 0;
		this.hour = 0;
		this.minute = 0;
		this.second = 0;	
	}
	
	public int getDay() {
		return day;
	}



	public void setDay(int day) {
		this.day = day;
	}



	public int getMonth() {
		return month-1;
	}



	public void setMonth(int month) {
		this.month = month;
	}



	public int getYear() {
		return year-1900;
	}



	public void setYear(int year) {
		this.year = year;
	}



	public int getHour() {
		return hour;
	}



	public void setHour(int hour) {
		this.hour = hour;
	}



	public int getMinute() {
		return minute;
	}



	public void setMinute(int minute) {
		this.minute = minute;
	}



	public int getSecond() {
		return second;
	}



	public void setSecond(int second) {
		this.second = second;
	}



	public String getDBDateTime(){

		String day = "" + this.day;
		String month = "" + this.month;
		String year = "" + this.year;
		String second = "" + this.second;
		String minute = "" + this.minute;
		String hour = "" + this.hour;

		if(day.length()==1){
			day="0"+day;
		}
		if(month.length()==1){
			month="0"+month;
		}
		if (second.length() == 1){
			second = "0" + second;
		}
		if (minute.length() == 1){
			minute = "0" + minute;
		}
		if (hour.length() == 1){
			hour = "0" + hour;
		}

		String dbdateTime = year + this.SPLITTER_DB_DATE + month + this.SPLITTER_DB_DATE + day + " " + hour + this.SPLITTER_TIME + minute + this.SPLITTER_TIME + second;
		return dbdateTime;
	}

	public String getItDate(){

		String day = "" + this.day;
		String month = "" + this.month;
		String year = "" + this.year;

		if(day.length()==1){
			day="0"+day;
		}
		if(month.length()==1){
			month="0"+month;
		}
		if (year.length() == 1){
			year = "0" + year;
		}

		String itdate = day + this.SPLITTER_IT_DATE + month + this.SPLITTER_IT_DATE + year;
		return itdate;
	}

	public String getItTime(){

		String second = "" + this.second;
		String minute = "" + this.minute;
		String hour = "" + this.hour;

		if (second.length() == 1){
			second = "0" + second;
		}
		if (minute.length() == 1){
			minute = "0" + minute;
		}
		if (hour.length() == 1){
			hour = "0" + hour;
		}

		String itTime = hour + this.SPLITTER_TIME + minute + this.SPLITTER_TIME + second;
		return itTime;
	}

	public void setDBdateTime(String DBdateTime){

		if (DBdateTime != null){

			StringTokenizer dateTime = new StringTokenizer(DBdateTime, " ");
			StringTokenizer date = new StringTokenizer(dateTime.nextToken(),this.SPLITTER_DB_DATE);
			StringTokenizer time = new StringTokenizer(dateTime.nextToken(),this.SPLITTER_TIME);

			this.year = Integer.parseInt(date.nextToken());
			this.month = Integer.parseInt(date.nextToken());
			this.day = Integer.parseInt(date.nextToken());

			this.hour = Integer.parseInt(time.nextToken());
			this.minute = Integer.parseInt(time.nextToken());
			int second = (int) Float.parseFloat(time.nextToken());
			this.second = second;
		}

	}

	public void setItDate(String ItDate){

		StringTokenizer date = new StringTokenizer(ItDate, this.SPLITTER_IT_DATE);

		this.day = Integer.parseInt(date.nextToken());
		this.month = Integer.parseInt(date.nextToken());
		this.year = Integer.parseInt(date.nextToken());
	}

	public void setItTime(String ItTime){

		StringTokenizer time = new StringTokenizer(ItTime, this.SPLITTER_TIME);

		this.hour = Integer.parseInt(time.nextToken());
		this.minute = Integer.parseInt(time.nextToken());
		this.second = Integer.parseInt(time.nextToken());
	}



	@SuppressWarnings("static-access")
	public void setCurrentdate(){

		java.util.TimeZone t = java.util.TimeZone.getTimeZone("ECT");
		java.util.Calendar now = java.util.Calendar.getInstance(t);

		this.day = now.get(now.DAY_OF_MONTH);
		this.month = (now.get(now.MONTH)+1);
		this.year = now.get(now.YEAR);
	}

	@SuppressWarnings("static-access")
	public void progressDate(int day){

		java.util.TimeZone t=java.util.TimeZone.getTimeZone("ECT");
		java.util.Calendar now = java.util.Calendar.getInstance(t);

		now.add(Calendar.DATE, day);

		this.day = now.get(now.DAY_OF_MONTH);
		this.month = (now.get(now.MONTH)+1);
		this.year = now.get(now.YEAR);
		this.second = now.get(now.SECOND);
		this.minute = now.get(now.MINUTE);
		this.hour = now.get(now.HOUR_OF_DAY);
	}

	@SuppressWarnings("static-access")
	public void setCurrentTime(){

		java.util.TimeZone t=java.util.TimeZone.getTimeZone("ECT");
		java.util.Calendar now = java.util.Calendar.getInstance(t);

		this.second = now.get(now.SECOND);
		this.minute = now.get(now.MINUTE);
		this.hour = now.get(now.HOUR_OF_DAY);
	}

	@SuppressWarnings("static-access")
	public void progressTime(int hour){

		java.util.TimeZone t=java.util.TimeZone.getTimeZone("ECT");
		java.util.Calendar now = java.util.Calendar.getInstance(t);

		now.add(Calendar.HOUR, hour);

		this.day = now.get(now.DAY_OF_MONTH);
		this.month = (now.get(now.MONTH)+1);
		this.year = now.get(now.YEAR);
		this.second = now.get(now.SECOND);
		this.minute = now.get(now.MINUTE);
		this.hour = now.get(now.HOUR_OF_DAY);
	}

	public boolean isExpired(){

		java.util.Calendar dateTime = java.util.Calendar.getInstance();
		dateTime.set(this.year, (this.month-1), this.day, this.hour, this.minute, this.second);
		java.util.TimeZone t=java.util.TimeZone.getTimeZone("ECT");
		java.util.Calendar now = java.util.Calendar.getInstance(t);

		int res = dateTime.compareTo(now);

		if (res > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	

	@Override
	public String toString() {

		String day = "" + this.day;
		String month = "" + this.month;
		String year = "" + this.year;
		String second = "" + this.second;
		String minute = "" + this.minute;
		String hour = "" + this.hour;

		if(day.length()==1){
			day="0"+day;
		}
		if(month.length()==1){
			month="0"+month;
		}
		if (year.length() == 1){
			year = "0" + year;
		}
		if (second.length() == 1){
			second = "0" + second;
		}
		if (minute.length() == 1){
			minute = "0" + minute;
		}
		if (hour.length() == 1){
			hour = "0" + hour;
		}
		return day + this.SPLITTER_IT_DATE + month + this.SPLITTER_IT_DATE + year + " " + hour + this.SPLITTER_TIME + minute + SPLITTER_TIME + second;
	}
}
