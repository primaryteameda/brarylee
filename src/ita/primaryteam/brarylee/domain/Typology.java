package ita.primaryteam.brarylee.domain;

//JAVABEAN PER MEMORIZZARE I DATI DELL'UTENTE ( RUOLO)
public class Typology {
	
	// ATTRIBBUTI
	private int id;
	private String role;
	private boolean visibility;
	static final private String FORMAT = "%-5s%-10s";

	// METODI
	public Typology() {
		this.setId(0);
		this.setRole("");
		this.setVisibility(true);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isVisible() {
		return visibility;
	}

	public void setVisibility(Boolean visibility) {
		this.visibility = visibility;
	}

	public static void stampaHeader() {
		String str = String.format(FORMAT, "ID", "RUOLO") ;
		System.out.println(str);
		str = Book.pad(20, '-');
	}
	
	public void stampaTypology() {
		String str = String.format(FORMAT, this.id, this.role);
		System.out.println(str);
	}
	
	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override
	public String toString() {
		return this.id + " " + this.role + " "+ this.visibility;
	}
}