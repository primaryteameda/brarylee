package ita.primaryteam.brarylee.domain;

import java.util.ArrayList;
import java.util.List;

//javaBean per memorizzare i dati del Libro
public class Book {

	// ATTRIBBUTI
	private int id;
	private String title;
	private String author;
	private String publisher;
	private Genre genre; // OGGETTO PER RICHIAMARE LA CLASSE "GENRE"
	private int year;
	private String edition;
	private String isbn;
	private List<Copy> copies;// LISTA DOVE SONO CONTENUTE LE COPIE ASSOCIATE AI LIBRI
	private String note;
	private boolean visibility;
	static final private String FORMAT = "%-5s%-50s%-40s%-25s%-20s%-10s%-15s%-20s%-15s%-10s";
	// METODI
	public Book() {

		this.setId(0);
		this.setTitle("");
		this.setAuthor("");
		this.setPublisher("");
		genre = new Genre();
		this.setYear(0);
		this.setEdition("");
		this.setIsbn("");
		this.copies = new ArrayList<Copy>();
		this.setNote("");
		this.setVisibility(true);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	// ottiene il titolo del Libro
	public String getTitle() {
		return title;
	}
	// imposta il nome del Libro
	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public boolean isVisible() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	public List<Copy> getCopies() {
		return copies;
	}

	public void setCopies(List<Copy> copies) {
		this.copies = copies;
	}
		
	public static void stampaHeader() {
		String str = String.format(FORMAT, "ID", "TITOLO", "AUTORE", "EDITORE", "GENERE", "ANNO", "EDIZIONE", "ISBN", "NOTE", "COPIE") ;
		System.out.println(str);
		str = pad(280, '-');
	}
	
	public void stampaLibro() {
		String str = String.format(FORMAT, this.id, this.title, this.author, this.publisher, this.genre.getName(), this.year, this.edition, this.isbn, (this.note != null ? this.note : ""), this.copies) ;
		System.out.println(str);
	}
	
	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override
	public String toString() {
		return " Titolo: " + this.title + " - Autore: " + this.author + " - Editore: " + this.publisher + " - Genere: " + this.genre.getName() + " - Anno: " +
				this.year + " - Edizione: " + this.edition + " - ISBN: " + this.isbn + (this.note != null ? " - Note: " + this.note : "");
	}
	
	public static String pad(int size, char c) {
		String str = "";
		String frm = "%" + size + "s";
		str=String.format(frm, " ").replace(' ', c);
		System.out.println(str);
		return str;
	}

}
