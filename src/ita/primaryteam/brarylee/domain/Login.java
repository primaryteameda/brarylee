package ita.primaryteam.brarylee.domain;

//JAVABEAN PER MEMORIZZARE I DATI DI ACCESSO DELL'UTENTE
public class Login {
	
	// ATTRIBBUTI	
	private int id;
	private String username;
	private String password;
	private User user;      // OGGETTO PER RICHIAMARE LA CLASSE "USER"
	private boolean visibility;
	
	// METODI
	public Login () {
		
		this.username = "";
		this.password = "";
		user = new User();
		this.visibility = true;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isVisible() {
		return visibility;
	}
	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}
	
	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override
	public String toString() 
	{
		return this.id + " " + this.username + " " + this.password + " " + this.visibility;

	}
}
