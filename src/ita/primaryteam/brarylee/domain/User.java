package ita.primaryteam.brarylee.domain;

import java.sql.Timestamp;

//JAVABEAN PER MEMORIZZARE I DATI PERSONALI DELL'UTENTE
public class User {
	
	// ATTRIBBUTI
	private String id;
	private String name;
	private String surname;
	private Timestamp birthDate;
	private String birthPlace;
	private String cf;
	private String document;
	private String numberDocument;
	private Timestamp dateDocument;
	private String release;
	private Typology typology;     // OGGETTO PER RICHIAMARE LA CLASSE "TYPOLOGY"
	private String level;
	private String address;
	private String city;
	private String telephone;
	private String email;
	private boolean enabled;
	private boolean visibility;
	static final private String FORMAT = "%-5s%-10s%-20s%-15s%-15s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-20s%-15s%-25s%-10s";

	// METODI
	public User() {
		
		this.setId(0);
		this.setName("");
		this.setSurname("");
		this.setBirthPlace("");
		this.setCf("");
		this.setDocument("");
		this.setNumberDocument("");
		this.setRelease("");
		typology = new Typology();
		this.setLevel("");
		this.setAddress("");
		this.setCity("");
		this.setTelephone("");
		this.setEmail("");
		this.setEnabled(true);
		this.setVisibility(true);
	}

	public int getId() {
		
		int id = Integer.parseInt(this.id);
		return id;
	}
	
	public String getStringId() {

		return this.id;
	}

	public void setId(int id) {
		String ids = "" + id;
		
		while(ids.length()<6){
			ids="0" + ids;
			this.id = ids;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Timestamp getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Timestamp birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}
	
	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getNumberDocument() {
		return numberDocument;
	}

	public void setNumberDocument(String numberDocument) {
		this.numberDocument = numberDocument;
	}

	public Timestamp getDateDocument() {
		return dateDocument;
	}

	public void setDateDocument(Timestamp dateDocument) {
		this.dateDocument = dateDocument;
	}

	public String getRelease() {
		return release;
	}

	public void setRelease(String release) {
		this.release = release;
	}

	public Typology getTypology() {
		return typology;
	}

	public void setTypology(Typology Typology) {
		this.typology = Typology;
	}
	
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isVisible() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}
	
	public static void stampaHeader() {
		String str = String.format(FORMAT, "ID", "LIVELLO", "TIPOLOGIA", "NOME", "COGNOME", "DATA NASCITA", "LUOGO NASCITA", "DOCUMENTO", "NUMERO", "DATA", "RILASCIATO DA", "INDIRIZZO", "CITT�", "TELEFONO", "E-MAIL", "ABILITATO") ;
		System.out.println(str);
		str = Book.pad(275, '-');
	}
	
	private String date() {
		int day = this.birthDate.getDate();
		int year = this.birthDate.getYear()+1900;
		int month=this.birthDate.getMonth()+1;
		return  day + "/" + month + "/" + year;
	}
	
	private String dateD() {
		int dayD = this.dateDocument.getDate();
		int yearD = this.dateDocument.getYear()+1900;
		int monthD= this.dateDocument.getMonth()+1;
		return  dayD + "/" + monthD + "/" + yearD;
	}
	
	private int code() {
		return Integer.valueOf(this.id);
	}
	
	public void stampaUser() {
		String str = String.format(FORMAT, code(), this.level, this.typology.getRole(), this.name, this.surname, date() , this.birthPlace, this.document, this.numberDocument, dateD(), this.release, this.address, this.city, this.telephone, this.email, (this.enabled ? "Si" : "No")) ;
		System.out.println(str);	
	}
	
	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override
	public String toString() 
	{
		return "Livello utente: " + this.level + " - Tipologia: " + this.typology.getRole() + " - Nome: " + this.name + " - Cognome: " + this.surname + " - Data nascita: " + this.birthDate + " - Luogo nascita: " + this.birthPlace + " - Documento: " + this.document + " - Numero: " + this.numberDocument + 
				" - Data: " + this.dateDocument + " - Rilasciato da: " + this.release + " - Indirizzo: " + this.address + " - Citt�: " + this.city + " - Telefono: " + this.telephone + " - E-Mail: " + this.email + " - Abilitato: " + (this.enabled ? "Si" : "No");
	}
}
