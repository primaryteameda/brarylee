package ita.primaryteam.brarylee.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ita.primaryteam.brarylee.domain.Book;
import ita.primaryteam.brarylee.domain.Copy;
import ita.primaryteam.brarylee.domain.DateTime;
import ita.primaryteam.brarylee.domain.Genre;
import ita.primaryteam.brarylee.domain.Handling;
import ita.primaryteam.brarylee.domain.Position;
import ita.primaryteam.brarylee.domain.Typology;
import ita.primaryteam.brarylee.domain.User;
import ita.primaryteam.brarylee.repository.DBRepository;

public class ManageHandling extends DBRepository implements Manage{

	public final static String LOAN = "loan";
	public final static String BOOKING = "booking";
	public final static String CONSULTATION = "consultation";

	public final int DATEEXPIRY = 90;  //per gestiore prenotazioni
	public final int TIMEEXPIRY = 24;

	public final String TYPE = "type"; // per ricerca
	public final String CF_USER = "u.cf";
	public final String ID_USER = "u.id";
	public final String ISBN = "b.isbn";
	public final String CODE = "c.code";
	public final String GENRE = "g.name";
	public final String TYPOLOGY = "role";
	public final String RESTITUTION = "restitution";

	public String type = "";

	private Copy copy= new Copy();
	private User user = new User();
	private Handling handling = new Handling();

	DateTime start = new DateTime();
	DateTime expiry = new DateTime();
	DateTime restitution = new DateTime();

	public ManageHandling( String type, Copy copy, User user) {

		this.copy = copy;
		this.user = user;
		this.type = type;
	}

	@Override
	public int update(int id) {

		return 0;
	}

	@Override
	public int add() {

		handling.setVisibility(true);

		int res = 0;

		start.setCurrentdate();
		start.setCurrentTime();

		switch(this.type) {

		case LOAN:
			expiry.progressDate(this.DATEEXPIRY);
			break;
		case BOOKING:
			expiry.progressTime(this.TIMEEXPIRY);
			break;
		case CONSULTATION:
			expiry.setCurrentdate();
			expiry.setItTime("22:00:00");                           
			break;
		}

		String sql = String.format("INSERT INTO Handlings (type, idcopy, iduser, start, expiry, visibility) " + 
				"VALUES ('%s', %d, %d, '%s', '%s', %b)",
				this.type, copy.getId(), user.getId(),start.getDBDateTime() , expiry.getDBDateTime(), handling.isVisible());

		res = req(sql);

		return res;
	}

	public int restitution (int id) {

		int res = 0;

		restitution.setCurrentdate();
		restitution.setCurrentTime();
		String sql = "UPDATE  Handlings SET restitution= '" + restitution.getDBDateTime() + "' WHERE id=" + id ;

		res = req(sql);

		disableUserLaggard();

		return res;
	}

	@Override
	public int delete(int id) {		

		int res = 0;

		String sql = "UPDATE  Handlings SET visibility= false WHERE id=" + id;

		res = req(sql);

		return res;
	}

	public List<Handling> getAllByType() {

		deleteExpired();
		disableUserLaggard();

		List<Handling> hands = new ArrayList <Handling> ();

		/*String sql = "SELECT * FROM Users as u, typologies as t, genres as g, books as b, copies as c, positions as p, handlings as h"
				+ "  WHERE u.idtypology = t.id  AND b.id= c.idbook AND b.idgenre = g.id AND c.idposition = p.id AND h.idcopy= c.id AND h.iduser=u.id AND h.visibility=true AND type='" + this.type +"'";
*/
		String sql = "SELECT h.*, " 
				   + "      u.id as userid, "
				   + "      u.*, "
			       + "      t.*, "
			       + "      c.id as copyid, "
			       + "      c.*, "
			       + "      p.id as positionid, "
			       + "      p.*, "
			       + "      b.id as bookid, "
			       + "      b.*, "
			       + "      g.id as genreid, "
			       + "      g.* "
			       + " FROM handlings h "
			       + " INNER JOIN users u ON h.iduser = u.id "
			       + "INNER JOIN typologies t ON u.idtypology = t.id "
			       + " INNER JOIN copies c ON h.idcopy = c.id "
			       + " INNER JOIN positions p ON c.idposition = p.id "
			       + " INNER JOIN books b ON c.idbook = b.id "
			       + " INNER JOIN genres g ON b.idgenre = g.id "
			       + " WHERE h.visibility = true "
				   + " AND type='" + this.type +"'";
		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {
			
			User user = new User();
			Typology typology = new Typology();

			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);

			user.setId((Integer)result.get("userid"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthDate"));
			user.setCf((String)result.get("cf"));
			user.setBirthPlace((String)result.get("birthPlace"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberDocument"));
			user.setDateDocument((Timestamp)result.get("dateDocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel((String)result.get("level"));
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			user.setEnabled(true);
			user.setVisibility(true);

			Book book = new Book();
			Genre genre = new Genre();
			List<Copy> copies = new ArrayList<Copy>();
			Handling hand = new Handling();


			Position position = new Position();
			Copy copy = new Copy();

			position.setId((Integer)result.get("positionid"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			copy.setId((Integer)result.get("copyid"));
			copy.setCode((String)result.get("code"));
			copy.setPosition(position);
			copy.setVisibility(true);
			copies.add(copy);

			genre.setId((Integer)result.get("genreid"));
			genre.setName((String)result.get("name"));
			genre.setVisibility(true);

			book.setId((Integer)result.get("bookid"));
			book.setTitle((String)result.get("title"));
			book.setAuthor((String)result.get("author"));
			book.setPublisher((String)result.get("publisher"));
			book.setGenre(genre);
			book.setYear((Integer)result.get("year"));
			book.setEdition((String)result.get("edition"));
			book.setIsbn((String)result.get("isbn"));
			book.setCopies(copies);
			book.setNote((String)result.get("note"));
			book.setVisibility(true);

			hand.setId((Integer)result.get("id"));
			hand.setType((String)result.get("type"));
			hand.setBook(book);
			hand.setUser(user);
			hand.setCopy(copy);
			hand.setStart((Timestamp)result.get("start"));
			hand.setExpiry((Timestamp)result.get("expiry"));
			hand.setRestitution((Timestamp)result.get("restitution"));
			hand.setVisibility(true);
			hands.add(hand);

			
		}

		return hands;
	}

	public List<Handling> getAll() {

		deleteExpired();
		disableUserLaggard();

		List<Handling> hands = new ArrayList <Handling> ();

		String sql = "SELECT h.*, "
				   + "	     u.id as userid, "
				   + "       u.*, "
				   + "       p.id as positionid, "
				   + "       p.*, " 
				   + "       c.id as copyid, " 
				   + "       c.*, "
				   + "       g.id as genreid, "
				   + "       g.*, "
				   + "       b.id as bookid, "
				   + "       b.* "
				   + "FROM handlings h" 
				   + "INNER JOIN users u ON h.iduser = u.id" 
				   + "INNER JOIN copies c ON h.idcopy = c.id" 
				   + "INNER JOIN positions p ON c.idposition = p.id"  
				   + "INNER JOIN books b ON c.idbook = b.id"
				   + "INNER JOIN genres g ON b.idgenre = g.id" 
				   + "WHERE h.visibility = true";

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Typology typology = new Typology();

			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);

			user.setId((Integer)result.get("userId"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthdate"));
			user.setCf((String)result.get("cf"));
			user.setBirthPlace((String)result.get("birthplace"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberdocument"));
			user.setDateDocument((Timestamp)result.get("datedocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel((String)result.get("level"));
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			user.setEnabled(true);
			user.setVisibility(true);

			Book book = new Book();
			Genre genre = new Genre();
			List<Copy> copies = new ArrayList<Copy>();
			Handling hand = new Handling();

			Position position = new Position();
			Copy copy = new Copy();

			position.setId((Integer)result.get("positionid"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			copy.setId((Integer)result.get("copyid"));
			copy.setCode((String)result.get("code"));
			copy.setPosition(position);
			copy.setVisibility(true);
			copies.add(copy);

			genre.setId((Integer)result.get("genreid"));
			genre.setName((String)result.get("name"));
			genre.setVisibility(true);

			book.setId((Integer)result.get("bookid"));
			book.setTitle((String)result.get("title"));
			book.setAuthor((String)result.get("author"));
			book.setPublisher((String)result.get("publisher"));
			book.setGenre(genre);
			book.setYear((Integer)result.get("year"));
			book.setEdition((String)result.get("edition"));
			book.setIsbn((String)result.get("isbn"));
			book.setCopies(copies);
			book.setNote((String)result.get("note"));
			book.setVisibility(true);

			hand.setId((Integer)result.get("id"));
			hand.setType((String)result.get("type"));
			hand.setBook(book);
			hand.setUser(user);
			hand.setStart((Timestamp)result.get("start"));
			hand.setExpiry((Timestamp)result.get("expiry"));
			hand.setRestitution((Timestamp)result.get("restitution"));
			hand.setVisibility(true);
			hands.add(hand);
		}
		return hands;
	}

	public List<Handling> getAllByUser(int iduser) {

		deleteExpired();
		disableUserLaggard();

		List<Handling> hands = new ArrayList <Handling> ();
		
		String sql = "SELECT h.*, "
				   + "	     u.id as userid, "
				   + "       u.*, "
				   + "       p.id as positionid, "
				   + "       p.*, " 
				   + "       c.id as copyid, " 
				   + "       c.*, "
				   + "       g.id as genreid, "
				   + "       g.*, "
				   + "       b.id as bookid, "
				   + "       b.* "
				   + "FROM handlings h " 
				   + "INNER JOIN users u ON h.iduser = u.id " 
				   + "INNER JOIN copies c ON h.idcopy = c.id " 
				   + "INNER JOIN positions p ON c.idposition = p.id "  
				   + "INNER JOIN books b ON c.idbook = b.id "
				   + "INNER JOIN genres g ON b.idgenre = g.id " 
				   + "WHERE h.visibility = true "
				   + "      AND type = '" + this.type + "' AND u.id = " + iduser;

		/* String sql = "SELECT * FROM Users as u, typologies as t, genres as g, books as b, copies as c, positions as p, handlings as h"
				+ "  WHERE u.idtypology = t.id  AND b.idgenre = g.id AND c.idposition = p.id AND h.idcopy= c.id AND h.iduser=u.id "
				+ "AND h.visibility=true AND type='" + this.type +"' AND u.id=" + iduser; */

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Typology typology = new Typology();

			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);

			user.setId((Integer)result.get("userId"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthdate"));
			user.setCf((String)result.get("cf"));
			user.setBirthPlace((String)result.get("birthplace"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberdocument"));
			user.setDateDocument((Timestamp)result.get("datedocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel((String)result.get("level"));
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			user.setEnabled(true);
			user.setVisibility(true);

			Book book = new Book();
			Genre genre = new Genre();
			List<Copy> copies = new ArrayList<Copy>();
			Handling hand = new Handling();

			Position position = new Position();
			Copy copy = new Copy();

			position.setId((Integer)result.get("positionid"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			copy.setId((Integer)result.get("copyid"));
			copy.setCode((String)result.get("code"));
			copy.setPosition(position);
			copy.setVisibility(true);
			copies.add(copy);

			genre.setId((Integer)result.get("genreid"));
			genre.setName((String)result.get("name"));
			genre.setVisibility(true);

			book.setId((Integer)result.get("bookid"));
			book.setTitle((String)result.get("title"));
			book.setAuthor((String)result.get("author"));
			book.setPublisher((String)result.get("publisher"));
			book.setGenre(genre);
			book.setYear((Integer)result.get("year"));
			book.setEdition((String)result.get("edition"));
			book.setIsbn((String)result.get("isbn"));
			book.setCopies(copies);
			book.setNote((String)result.get("note"));
			book.setVisibility(true);

			hand.setId((Integer)result.get("id"));
			hand.setType((String)result.get("type"));
			hand.setBook(book);
			hand.setUser(user);
			hand.setStart((Timestamp)result.get("start"));
			hand.setExpiry((Timestamp)result.get("expiry"));
			hand.setRestitution((Timestamp)result.get("restitution"));
			hand.setVisibility(true);
			hands.add(hand);
		}

		return hands;
	}

	public int deleteExpired(){

		int res = 0;
		DateTime dt = new DateTime();

		dt.setCurrentdate();
		dt.setCurrentTime();

		String sql="UPDATE Handlings as h SET visibility= false WHERE type= 'booking' AND expiry < '" + dt.getDBDateTime() + "'";

		res= req(sql);

		return res;		
	}

	public int disableUserLaggard(){

		int res = 0;
		String sql;

		sql = "SELECT u.id, COUNT(u.name) as delay FROM Users as u, typologies as t, genres as g, books as b, copies as c, positions as p, handlings as h  "
				+ "WHERE u.idtypology = t.id  AND b.idgenre = g.id AND c.idposition = p.id AND h.idcopy= c.id AND h.iduser=u.id AND "
				+ "h.restitution > h.expiry AND h.type = 'loan' AND h.visibility=true GROUP BY u.id ";

		List<Map<String, Object>> results = select(sql);

		Integer id;
		Long delay;

		for(Map<String, Object> result : results) {
			id = (Integer)result.get("id");	
			delay = (Long)result.get("delay");	

			if ( delay >= 5){
				sql="UPDATE users as u SET enabled= false WHERE u.id = " + id;
				res= req(sql);
			}

		}


		return res;		
	}


	public List<Handling> searchHandling(String column, String value) {

		deleteExpired();
		disableUserLaggard();

		List<Handling> hands = new ArrayList <Handling> ();

		value = value.replace("'", "''");

		String sql;

		if (column.equals(RESTITUTION)){
			sql = 	" SELECT h.*, " + 
					"	     u.id as userid, " + 
					"        u.*, " + 
					"        t.*, " + 
					"        c.id as copyid, " + 
					"        c.*, " + 
					"        p.id as positionid, " + 
					"        p.*, " + 
					"        b.id as bookid, " + 
					"        b.*, " + 
					"        g.id as genreid, " + 
					"        g.* " + 
					" FROM handlings h " + 
					" INNER JOIN users u ON h.iduser = u.id " + 
					" INNER JOIN typologies t ON u.idtypology = t.id " + 
					" INNER JOIN copies c ON h.idcopy = c.id " + 
					" INNER JOIN positions p ON c.idposition = p.id " + 
					" INNER JOIN books b ON c.idbook = b.id " + 
					" INNER JOIN genres g ON b.idgenre = g.id " + 
					" WHERE h.visibility IS TRUE " + 
					"       AND " + column + " is null";
		}
		else{
			sql = 	" SELECT h.*, " + 
					"	     u.id as userid, " + 
					"        u.*, " + 
					"        t.*, " + 
					"        c.id as copyid, " + 
					"        c.*, " + 
					"        p.id as positionid, " + 
					"        p.*, " + 
					"        b.id as bookid, " + 
					"        b.*, " + 
					"        g.id as genreid, " + 
					"        g.* " + 
					" FROM handlings h " + 
					" INNER JOIN users u ON h.iduser = u.id " + 
					"  INNER JOIN typologies t ON u.idtypology = t.id " + 
					" INNER JOIN copies c ON h.idcopy = c.id " + 
					" INNER JOIN positions p ON c.idposition = p.id " + 
					" INNER JOIN books b ON c.idbook = b.id " + 
					" INNER JOIN genres g ON b.idgenre = g.id " + 
					"WHERE h.visibility IS TRUE " +
				    "      AND " + column + " = '" + value + "'";
		}

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Typology typology = new Typology();

			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);

			user.setId((Integer)result.get("userid"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthDate"));
			user.setCf((String)result.get("cf"));
			user.setBirthPlace((String)result.get("birthPlace"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberDocument"));
			user.setDateDocument((Timestamp)result.get("dateDocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel((String)result.get("level"));
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			user.setEnabled(true);
			user.setVisibility(true);

			Book book = new Book();
			Genre genre = new Genre();
			List<Copy> copies = new ArrayList<Copy>();
			Handling hand = new Handling();


			Position position = new Position();
			Copy copy = new Copy();

			position.setId((Integer)result.get("positionid"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			copy.setId((Integer)result.get("copyid"));
			copy.setCode((String)result.get("code"));
			copy.setPosition(position);
			copy.setVisibility(true);
			copies.add(copy);

			genre.setId((Integer)result.get("genreid"));
			genre.setName((String)result.get("name"));
			genre.setVisibility(true);

			book.setId((Integer)result.get("bookid"));
			book.setTitle((String)result.get("title"));
			book.setAuthor((String)result.get("author"));
			book.setPublisher((String)result.get("publisher"));
			book.setGenre(genre);
			book.setYear((Integer)result.get("year"));
			book.setEdition((String)result.get("edition"));
			book.setIsbn((String)result.get("isbn"));
			book.setCopies(copies);
			book.setNote((String)result.get("note"));
			book.setVisibility(true);

			hand.setId((Integer)result.get("id"));
			hand.setType((String)result.get("type"));
			hand.setBook(book);
			hand.setUser(user);
			hand.setCopy(copy);
			hand.setStart((Timestamp)result.get("start"));
			hand.setExpiry((Timestamp)result.get("expiry"));
			hand.setRestitution((Timestamp)result.get("restitution"));
			hand.setVisibility(true);
			hands.add(hand);
		}

		return hands;
	}

	public List<Handling> searchNotRestitution(String column, String value) {

		deleteExpired();
		disableUserLaggard();

		List<Handling> hands = new ArrayList <Handling> ();

		value = value.replace("'", "''");

		String sql;

		sql = " SELECT h.*, " + 
				"	     u.id as userid, " + 
				"        u.*, " + 
				"        t.*, " + 
				"        c.id as copyid, " + 
				"        c.*, " + 
				"        p.id as positionid, " + 
				"        p.*, " + 
				"        b.id as bookid, " + 
				"        b.*, " + 
				"        g.id as genreid, " + 
				"        g.* " + 
				" FROM handlings h " + 
				" INNER JOIN users u ON h.iduser = u.id " + 
				" INNER JOIN typologies t ON u.idtypology = t.id " + 
				" INNER JOIN copies c ON h.idcopy = c.id " + 
				" INNER JOIN positions p ON c.idposition = p.id " + 
				" INNER JOIN books b ON c.idbook = b.id " + 
				" INNER JOIN genres g ON b.idgenre = g.id " + 
				" WHERE h.visibility IS TRUE " + 
			    "       AND h.restitution is null " +
			    "	    AND " + column + " = '" + value + "'";
		
		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Typology typology = new Typology();

			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);

			user.setId((Integer)result.get("userid"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthDate"));
			user.setCf((String)result.get("cf"));
			user.setBirthPlace((String)result.get("birthPlace"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberDocument"));
			user.setDateDocument((Timestamp)result.get("dateDocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel((String)result.get("level"));
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			user.setEnabled(true);
			user.setVisibility(true);

			Book book = new Book();
			Genre genre = new Genre();
			List<Copy> copies = new ArrayList<Copy>();
			Handling hand = new Handling();


			Position position = new Position();
			Copy copy = new Copy();

			position.setId((Integer)result.get("positionid"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			copy.setId((Integer)result.get("copyid"));
			copy.setCode((String)result.get("code"));
			copy.setPosition(position);
			copy.setVisibility(true);
			copies.add(copy);

			genre.setId((Integer)result.get("genreid"));
			genre.setName((String)result.get("name"));
			genre.setVisibility(true);

			book.setId((Integer)result.get("bookid"));
			book.setTitle((String)result.get("title"));
			book.setAuthor((String)result.get("author"));
			book.setPublisher((String)result.get("publisher"));
			book.setGenre(genre);
			book.setYear((Integer)result.get("year"));
			book.setEdition((String)result.get("edition"));
			book.setIsbn((String)result.get("isbn"));
			book.setCopies(copies);
			book.setNote((String)result.get("note"));
			book.setVisibility(true);

			hand.setId((Integer)result.get("id"));
			hand.setType((String)result.get("type"));
			hand.setBook(book);
			hand.setUser(user);
			hand.setCopy(copy);
			hand.setStart((Timestamp)result.get("start"));
			hand.setExpiry((Timestamp)result.get("expiry"));
			hand.setRestitution((Timestamp)result.get("restitution"));
			hand.setVisibility(true);
			hands.add(hand);
		}

		return hands;
	}

	public List<Handling> suspendedByUser(int id) {   // se l'utente ha qualcosa in sospeso

		List<Handling> hands = new ArrayList <Handling> ();

		String sql;
		
		sql= "SELECT h.*, " + 
				"	 u.id as userid, " + 
				"    u.*, " + 
				"    t.*, " + 
				"    c.id as copyid, " + 
				"    c.*, " + 
				"    p.id as positionid," + 
				"    p.*, " + 
				"    b.id as bookid, " + 
				"    b.*, " + 
				"    g.id as genreid, " + 
				"    g.* " + 
				" FROM handlings h " + 
				" INNER JOIN users u ON h.iduser = u.id " + 
				" INNER JOIN typologies t ON u.idtypology = t.id " + 
				" INNER JOIN copies c ON h.idcopy = c.id " + 
				" INNER JOIN positions p ON c.idposition = p.id " + 
				" INNER JOIN books b ON c.idbook = b.id " + 
				" INNER JOIN genres g ON b.idgenre = g.id " + 
				" WHERE h.visibility IS TRUE "
			  + "       AND restitution IS NULL AND type='" +  this.type + "' AND iduser = " + id ;
		
		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results){
			
			User user = new User();
			Typology typology = new Typology();

			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);

			user.setId((Integer)result.get("userid"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthDate"));
			user.setCf((String)result.get("cf"));
			user.setBirthPlace((String)result.get("birthPlace"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberDocument"));
			user.setDateDocument((Timestamp)result.get("dateDocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel((String)result.get("level"));
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			user.setEnabled(true);
			user.setVisibility(true);

			Book book = new Book();
			Genre genre = new Genre();
			List<Copy> copies = new ArrayList<Copy>();
			Handling hand = new Handling();


			Position position = new Position();
			Copy copy = new Copy();

			position.setId((Integer)result.get("positionid"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			copy.setId((Integer)result.get("copyid"));
			copy.setCode((String)result.get("code"));
			copy.setPosition(position);
			copy.setVisibility(true);
			copies.add(copy);

			genre.setId((Integer)result.get("genreid"));
			genre.setName((String)result.get("name"));
			genre.setVisibility(true);

			book.setId((Integer)result.get("bookid"));
			book.setTitle((String)result.get("title"));
			book.setAuthor((String)result.get("author"));
			book.setPublisher((String)result.get("publisher"));
			book.setGenre(genre);
			book.setYear((Integer)result.get("year"));
			book.setEdition((String)result.get("edition"));
			book.setIsbn((String)result.get("isbn"));
			book.setCopies(copies);
			book.setNote((String)result.get("note"));
			book.setVisibility(true);

			hand.setId((Integer)result.get("id"));
			hand.setType((String)result.get("type"));
			hand.setBook(book);
			hand.setUser(user);
			hand.setCopy(copy);
			hand.setStart((Timestamp)result.get("start"));
			hand.setExpiry((Timestamp)result.get("expiry"));
			hand.setRestitution((Timestamp)result.get("restitution"));
			hand.setVisibility(true);
			hands.add(hand);
			
		}

		return hands;

	}

	///////////////////////////////////ricerca  sospesi per copie

	public List<Handling> suspendedByCopy (int idcopy) {   // se un libro ha qualcosa in sospeso
		List<Handling> hands = new ArrayList <Handling> ();

		String sql;

		/* sql = "SELECT * FROM Users as u, typologies as t, genres as g, books as b, copies as c, positions as p, handlings as h"
				+ "  WHERE u.idtypology = t.id  AND b.idgenre = g.id AND c.idposition = p.id AND h.idcopy= c.id AND h.iduser=u.id AND h.visibility=true "
				+ "AND restitution IS NULL AND type='" +  this.type + "' AND idcopy = " + idcopy ; */
		
		sql = "SELECT h.*, " + 
				"	 u.id as userid, " + 
				"    u.*, " + 
				"    t.*, " + 
				"    c.id as copyid, " + 
				"    c.*, " + 
				"    p.id as positionid," + 
				"    p.*, " + 
				"    b.id as bookid, " + 
				"    b.*, " + 
				"    g.id as genreid, " + 
				"    g.* " + 
				" FROM handlings h " + 
				" INNER JOIN users u ON h.iduser = u.id " + 
				" INNER JOIN typologies t ON u.idtypology = t.id " + 
				" INNER JOIN copies c ON h.idcopy = c.id " + 
				" INNER JOIN positions p ON c.idposition = p.id " + 
				" INNER JOIN books b ON c.idbook = b.id " + 
				" INNER JOIN genres g ON b.idgenre = g.id " + 
				" WHERE h.visibility IS TRUE "
			  + "       AND restitution IS NULL AND type='" +  this.type + "' AND idcopy = " + idcopy;

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();

			Typology typology = new Typology();

			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);


			user.setId((Integer)result.get("userid"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthdate"));
			user.setCf((String)result.get("cf"));
			user.setBirthPlace((String)result.get("birthplace"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberdocument"));
			user.setDateDocument((Timestamp)result.get("datedocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel((String)result.get("level"));
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			user.setEnabled(true);
			user.setVisibility(true);

			Book book = new Book();
			Genre genre = new Genre();
			List<Copy> copies = new ArrayList<Copy>();
			Handling hand = new Handling();


			Position position = new Position();
			Copy copy = new Copy();

			position.setId((Integer)result.get("positionid"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			copy.setId((Integer)result.get("copyid"));
			copy.setCode((String)result.get("code"));
			copy.setPosition(position);
			copy.setVisibility(true);
			copies.add(copy);

			genre.setId((Integer)result.get("genreid"));
			genre.setName((String)result.get("name"));
			genre.setVisibility(true);

			book.setId((Integer)result.get("bookid"));
			book.setTitle((String)result.get("title"));
			book.setAuthor((String)result.get("author"));
			book.setPublisher((String)result.get("publisher"));
			book.setGenre(genre);
			book.setYear((Integer)result.get("year"));
			book.setEdition((String)result.get("edition"));
			book.setIsbn((String)result.get("isbn"));
			book.setCopies(copies);
			book.setNote((String)result.get("note"));
			book.setVisibility(true);

			hand.setId((Integer)result.get("id"));
			hand.setType((String)result.get("type"));
			hand.setBook(book);
			hand.setUser(user);
			hand.setStart((Timestamp)result.get("start"));
			hand.setExpiry((Timestamp)result.get("expiry"));
			hand.setRestitution((Timestamp)result.get("restitution"));
			hand.setVisibility(true);
			hands.add(hand);
		}

		return hands;
	}
}