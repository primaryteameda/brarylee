package ita.primaryteam.brarylee.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ita.primaryteam.brarylee.domain.Book;
import ita.primaryteam.brarylee.domain.Copy;
import ita.primaryteam.brarylee.domain.Handling;
import ita.primaryteam.brarylee.domain.Position;
import ita.primaryteam.brarylee.domain.User;
import ita.primaryteam.brarylee.repository.DBRepository;


//QUESTA CLASSE GESTISCE LE COPIE DI UN LIBRO
public class ManageCopy extends DBRepository implements Manage {

	public final String CODE = "code";

	private Book book = new Book();

	public ManageCopy(Book book) {

		this.book = book;
	}

	// METODO PER MODIFICARE LE COPIE DI UN LIBRO
	@Override
	public int update(int id) {

		int res = 0;

		book.getCopies().get(0).setCode(book.getCopies().get(0).getCode().replace("'", "''"));

		String sql = String.format("UPDATE copies c " +
				"  SET code = '%s', idposition = %d, c.visibility = true " +"WHERE id = %d",
				book.getCopies().get(0).getCode(), book.getCopies().get(0).getPosition().getId(), id);

		res = req(sql);

		return res;
	}

	// METODO PER CANCELLARE LE COPIE DI UN LIBRO
	@Override
	public int delete(int id) {      //////////////cancellare mcopy dato in input idcopy

		int res = 0;

		User user= new User();
		Copy copy = new Copy();

		copy.setId(id);
		List<Handling> handb = new ArrayList<Handling>();
		List<Handling> handc = new ArrayList<Handling>();
		List<Handling> handl = new ArrayList<Handling>();

		List<Handling> hands = new ArrayList<Handling>();


		ManageHandling mhb = new ManageHandling(ManageHandling.BOOKING,copy, user );
		ManageHandling mhc = new ManageHandling(ManageHandling.CONSULTATION, copy, user);
		ManageHandling mhl = new ManageHandling(ManageHandling.LOAN, copy , user);


		hands=mhb.suspendedByCopy(id);

		for(int j=0; j< hands.size(); j++){

			handb.add(hands.get(j)) ;
		}


		hands= mhc.suspendedByCopy(id);



		for(int j=0; j< hands.size(); j++){

			handc.add(hands.get(j)) ;
		}
		hands= mhl.suspendedByCopy(id);

		for(int j=0; j< hands.size(); j++){

			handl.add(hands.get(j)) ;
		}

		if (handb.size() > 0 || handc.size() > 0 || handl.size() > 0){
			return 1;
		}
		else{

			String sql = String.format("UPDATE copies c  SET c.visibility = false WHERE id = %d", id);

			res = req(sql);

			sql = "SELECT * FROM copies c where c.visibility = true AND c.idbook =" + book.getId();

			List<Map<String, Object>> results = select(sql);


			if (!results.isEmpty()) {
				return 0;
			}
			else {

				sql = String.format("UPDATE books b  SET b.visibility = false WHERE id = %d", book.getId());

				res = res + req(sql);
			}

			return res;
		}
	}

	// METODO PER AGGIUNGERE UNA COPIA A UN LIBRO
	@Override
	public int add() {

		int res = 0;

		book.getCopies().get(0).setCode(book.getCopies().get(0).getCode().replace("'", "''"));

		List<Copy> copies = new ArrayList<Copy>();

		copies = this.searchCopy(this.CODE, book.getCopies().get(0).getCode());

		if (copies.size() > 0) {
			return 1;
		}
		else {
			String sql = String.format("INSERT INTO Copies(code, idposition, idbook, visibility) " + 
					"VALUES ('%s', %d, %d, true)", book.getCopies().get(0).getCode(), book.getCopies().get(0).getPosition().getId(), 
					book.getId());

			res = req(sql);

		}

		return res;
	}

	// METODO DI RICERCA DELLE COPIE  DI UN LIBRO
	public List<Copy> getAll() {

		String sql = "SELECT c.*, "
				+ "       p.bookcase, "
				+ "       p.shelf "
				+ "    FROM copies as c "
				+ "    INNER JOIN positions as p "
				+ "    WHERE c.idposition = p.id AND c.visibility=true AND c.idbook = " + book.getId();

		List<Copy> copies = new ArrayList<Copy>();

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results){

			Position position = new Position();
			Copy copy = new Copy();

			position.setId((Integer)result.get("idposition"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			copy.setId((Integer)result.get("id"));
			copy.setCode((String)result.get("code"));
			copy.setPosition(position);
			copy.setVisibility(true);

			copies.add(copy);
		}

		return copies;
	}

	// METODO DI RICERCA  
	public List<Copy> searchCopy(String column, String value) {

		value.replace("'", "''");

		String sql = "SELECT * FROM copies as c "
				  +  "INNER JOIN positions as p "
				  +  "WHERE c.idposition = p.id AND c.visibility=true AND " + column + " = '" + value + "'";

		List<Copy> copies = new ArrayList<Copy>();

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results){

			Position position = new Position();
			Copy copy = new Copy();

			position.setId((Integer)result.get("idposition"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			copy.setId((Integer)result.get("id"));
			copy.setCode((String)result.get("code"));
			copy.setPosition(position);
			copy.setVisibility(true);

			copies.add(copy);
		}

		return copies;
	}
}
