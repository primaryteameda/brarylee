package ita.primaryteam.brarylee.logic;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ita.primaryteam.brarylee.domain.Genre;
import ita.primaryteam.brarylee.repository.DBRepository;

//QUESTA CLASSE GESTISCE I GENERI DI UN LIBRO
public class ManageGenre extends DBRepository implements Manage {

	public final String NAME = "name";

	public Genre newGenre= new Genre();

	private Genre genre = new Genre();


	public ManageGenre(Genre genre) {

		this.genre = genre;
	}
	public ManageGenre() {
	}


	@Override
	public int update(int id) {

		int res = 0;

		genre.setName(genre.getName().replace("'", "''"));

		String sql = String.format("UPDATE genres g " +
				"  SET g.name = '%s'," + " g.visibility = %b " +"WHERE g.id = %d",
				genre.getName(),
				genre.isVisible(), id);

		res = req(sql);

		return res;
	}

	@Override
	public int delete(int id) {

		int res = 0;

		String sql = String.format("UPDATE genres g  SET g.visibility = false WHERE g.id = %d", id);

		res = req(sql);

		if (res == 0){

			sql = String.format("UPDATE Books b SET b.idgenre = " + this.newGenre.getId() + " WHERE idgenre = %d", id);

			res = res + req(sql);
		}
		if (res != 0){
			sql = String.format("UPDATE Books b SET b.visibility = true WHERE id = %d", id);

			res = res + req(sql);	
		}
		return 0;
	}

	public List<Genre> getAll() {

		String sql = String.format("SELECT * FROM genres g where g.visibility = true");


		List<Genre>genres = new ArrayList<Genre>();

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Genre genre = new Genre();

			genre.setId((Integer)result.get("id"));
			genre.setName((String)result.get("name"));
			genre.setVisibility(true);

			genres.add(genre);
		}


		return genres;
	}

	public List<Genre> searchGenre(String column, String value) {

		value.replace("'", "''");

		String sql = "SELECT * FROM genres g where g.visibility = true AND " + column + " = '" + value + "'";


		List<Genre>genres = new ArrayList<Genre>();

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Genre genre = new Genre();

			genre.setId((Integer)result.get("g.id"));
			genre.setName((String)result.get("g.name"));
			genre.setVisibility(true);

			genres.add(genre);
		}


		return genres;
	}

	@Override
	public int add() {

		int res = 0;
		List<Genre>genres = new ArrayList<Genre>();

		genre.setName(genre.getName().replace("'", "''"));

		genres = this.searchGenre(this.NAME, genre.getName());

		if (genres.size() > 0) {
			return 1;
		}
		else {

			String sql = String.format("INSERT INTO Genres(name, visibility) " + 
					"VALUES ('%s',%b)", genre.getName(),
					genre.isVisible());

			res = req(sql);
		}	
		return res;
	}
}
