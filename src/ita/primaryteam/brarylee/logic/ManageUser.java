package ita.primaryteam.brarylee.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ita.primaryteam.brarylee.domain.Typology;
import ita.primaryteam.brarylee.domain.User;
import ita.primaryteam.brarylee.repository.DBRepository;

public class ManageUser extends DBRepository implements Manage {

	public final String SURNAME = "surname";
	public final String CF = "cf";
	public final String ID = "u.id";
	public final String CITY = "city";
	public final String TYPOLOGY = "role";
	public final String ENABLED = "enabled";


	User user = new User();

	DBRepository dbr = new DBRepository();

	public ManageUser(User user) {

		this.user = user;

	}

	@Override
	public int add() {

		int res = 0;

		List<User> users = new ArrayList<User>();

		users = this.searchUser(this.CF, user.getCf());

		if (users.size() > 0) {
			return 1;
		}
		else {

			user.setName(user.getName().replace ("'", "''"));
			user.setSurname(user.getSurname().replace ("'", "''"));
			user.setBirthPlace(user.getBirthPlace().replace ("'", "''"));
			user.setCf(user.getCf().replace ("'", "''"));
			user.setDocument(user.getDocument().replace ("'", "''"));
			user.setNumberDocument(user.getNumberDocument().replace ("'", "''"));
			user.setRelease(user.getRelease().replace ("'", "''"));
			user.setLevel(user.getLevel().replace ("'", "''"));
			user.setAddress(user.getAddress().replace ("'", "''"));
			user.setCity(user.getCity().replace ("'", "''"));
			user.setTelephone(user.getTelephone().replace ("'", "''"));
			user.setEmail(user.getEmail().replace ("'", "''"));

			String sql = String.format("INSERT INTO Users (name, surname, birthdate, birthplace, cf, document, numberdocument, datedocument, `release`, idtypology,"
					+ " level, address, city, telephone, email, enabled, visibility) VALUES "
					+ "('%s','%s','%s', '%s','%s','%s','%s','%s','%s', %d,'%s','%s','%s','%s', '%s', %b , %b)", 
					user.getName(), user.getSurname(), user.getBirthDate(), user.getBirthPlace(), user.getCf(), user.getDocument(), 
					user.getNumberDocument(), user.getDateDocument(), user.getRelease(), user.getTypology().getId(), user.getLevel(), 
					user.getAddress(), user.getCity(), user.getTelephone(), user.getEmail(), user.isEnabled(), user.isVisible());

			res = req(sql);

			return res; 

		}
	}

	@Override
	public int update(int id) {

		int res = 0;

		user.setName(user.getName().replace ("'", "''"));
		user.setSurname(user.getSurname().replace ("'", "''"));
		user.setBirthPlace(user.getBirthPlace().replace ("'", "''"));
		user.setCf(user.getCf().replace ("'", "''"));
		user.setDocument(user.getDocument().replace ("'", "''"));
		user.setNumberDocument(user.getNumberDocument().replace ("'", "''"));
		user.setRelease(user.getRelease().replace ("'", "''"));
		user.setLevel(user.getLevel().replace ("'", "''"));
		user.setAddress(user.getAddress().replace ("'", "''"));
		user.setCity(user.getCity().replace ("'", "''"));
		user.setTelephone(user.getTelephone().replace ("'", "''"));
		user.setEmail(user.getEmail().replace ("'", "''"));

		String sql = String.format("UPDATE Users u  SET name = '%s', surname = '%s', birthDate = '%s', cf = '%s', birthPlace = '%s', document = '%s', numberDocument = '%s',"
				+ " dateDocument = '%s' , `release` = '%s', idtypology = %d, level = '%s', address = '%s', city = '%s', telephone = '%s',"
				+ " email = '%s', enabled = %b, visibility = %b"  
				+ " WHERE u.id = %d", user.getName(), user.getSurname(), user.getBirthDate(), user.getCf(), user.getBirthPlace(), user.getDocument(), user.getNumberDocument(),
				user.getDateDocument(), user.getRelease(), user.getTypology().getId(), user.getLevel(), user.getAddress(),
				user.getCity(), user.getTelephone(), user.getEmail(), user.isEnabled(), user.isVisible(), id);

		res = req(sql);

		return res;
	}

	@Override
	public int delete(int id) {

		int res = 0;

		String sql = String.format("UPDATE Users u SET u.visibility = false WHERE id = %d", id);

		res = req(sql);

		return res;
	}


	public List<User> getAll() {

		List<User> users = new ArrayList <User> ();

		//String sql = String.format("SELECT * FROM Users as u, typologies as t WHERE u.idtypology = t.id AND u.visibility=true");

		String sql = "SElECT * " + 
				     "FROM users as u " + 
				     "INNER JOIN typologies t ON u.idtypology=t.id " + 
				     "WHERE u.visibility=true";
				
		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Typology typology = new Typology();

			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);

			user.setId((Integer)result.get("id"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthdate"));
			user.setCf((String)result.get("cf"));
			user.setBirthPlace((String)result.get("birthplace"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberdocument"));
			user.setDateDocument((Timestamp)result.get("datedocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel((String)result.get("level"));
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			if ((Integer)result.get("enabled")==0)
				user.setEnabled(false);
			else
				user.setEnabled(true);
			if ((Integer)result.get("visibility")==0)
				user.setVisibility(false);
			else
				user.setVisibility(true);

			users.add(user);
		}

		return users;
	}

	public List<User> searchUser(String column, String value) {

		List<User> users = new ArrayList <User> ();

		String sql;

		if (column.equals("enabled")) {

			sql = "SElECT * " + 
				     "FROM users as u " + 
				     "INNER JOIN typologies t ON u.idtypology=t.id " + 
				     "WHERE u.visibility=true AND u.enabled= " +  value.equals("true");
			
		} else {

			value = value.replace("'", "''");
			sql = "SELECT * "
				+ "FROM Users as u "
				+ "INNER JOIN typologies as t "
				+ "WHERE u.idtypology = t.id AND u.visibility=true AND " + column + " = '" + value + "'";
		}

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Typology typology = new Typology();

			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);

			user.setId((Integer)result.get("id"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthdate"));
			user.setCf((String)result.get("cf"));
			user.setBirthPlace((String)result.get("birthplace"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberdocument"));
			user.setDateDocument((Timestamp)result.get("datedocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel((String)result.get("level"));
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			user.setEnabled(true);
			user.setVisibility(true);

			users.add(user);
		}
		return users;
	}


	public int enabling (int id) {

		int res = 0;

		String sql = String.format("UPDATE Users u SET u.enabled = %b WHERE id = %d",user.isEnabled(), id);


		res= req(sql);

		return res;  
	}




}
