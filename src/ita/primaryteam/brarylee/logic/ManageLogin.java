package ita.primaryteam.brarylee.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ita.primaryteam.brarylee.domain.Copy;
import ita.primaryteam.brarylee.domain.DateTime;
import ita.primaryteam.brarylee.domain.Handling;
import ita.primaryteam.brarylee.domain.Login;
import ita.primaryteam.brarylee.domain.Typology;
import ita.primaryteam.brarylee.domain.User;
import ita.primaryteam.brarylee.repository.DBRepository;

public class ManageLogin extends DBRepository implements Manage{

	public final String USERNAME = "username";
	public final String EMAIL = "email";

	private Login login = new Login();
	private User user = new User();

	public ManageLogin(Login login) {
		this.login = login;
	}

	public User getLogin() {


		//user sbagliato
		user.setLevel("User Unknown");
		user.setVisibility(false);

		// uno dei due errato
		String sql = "SELECT *     "
				   + "  FROM Users as u "   
				   + "  INNER JOIN typologies as t ON u.idtypology = t.id  "
				   + "  INNER JOIN login as l ON u.id = l.iduser "
	               + "  WHERE l.visibility = true "
	               + "        AND l.username = '" + login.getUsername() + "'";
		
		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {
			//username giusto pass sbagliata
			Typology typology = new Typology();
			typology.setId((Integer)result.get("idtypology"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);

			user.setId((Integer)result.get("id"));
			user.setName((String)result.get("name"));
			user.setSurname((String)result.get("surname"));
			user.setBirthDate((Timestamp)result.get("birthdate"));
			user.setBirthPlace((String)result.get("birthplace"));
			user.setCf((String)result.get("cf"));
			user.setDocument((String)result.get("document"));
			user.setNumberDocument((String)result.get("numberdocument"));

			user.setDateDocument((Timestamp)result.get("datedocument"));
			user.setRelease((String)result.get("release"));
			user.setTypology(typology);
			user.setLevel("Password Error");
			user.setAddress((String)result.get("address"));
			user.setCity((String)result.get("city"));
			user.setTelephone((String)result.get("telephone"));
			user.setEmail((String)result.get("email"));
			user.setEnabled(((Integer)result.get("enabled")) == 1);
			user.setVisibility(false);

			sql = "SELECT *  "
			    + "  FROM Users as u "
			    + "  INNER JOIN Login as l ON u.id = l.iduser "
			    + "  WHERE l.visibility = true"
			    + "        AND l.username ='" + login.getUsername() + "'"
			    + "        AND l.password ='"  + login.getPassword() + "'";

			List<Map<String, Object>> results2 = select(sql);	

			for(Map<String, Object> result2 : results2) {

				// user giusto pass giusta
				user.setLevel((String)result2.get("level"));
				user.setVisibility(true);
			}
		}

		return user;
	}

	@Override
	public int update(int id) {

		int res = 0;

		String sql = "SELECT * FROM login as l WHERE l.visibility=true AND username ='" + login.getUsername() + "' AND id <> " + id;


		List<Map<String, Object>> results = select(sql);

		if (!results.isEmpty()) {
			return 2;
		}
		else {

			ManageUser mu = new ManageUser(login.getUser());

			sql = "SELECT * FROM Users as u WHERE u.visibility=true AND cf ='" + login.getUser().getCf() + "' AND id <> " + login.getUser().getId();



			List<Map<String, Object>> results2 = select(sql);

			
				if (!results2.isEmpty()) {
					return 1;
				}
				else {

					login.setUsername (login.getUsername().replace ("'", "''"));
					login.setPassword (login.getPassword().replace ("'", "''"));

					sql = String.format("UPDATE Login l SET username = '%s', password = '%s' WHERE l.id = %d", login.getUsername(), login.getPassword(), id);

					res = req(sql);

					mu = new ManageUser(login.getUser());

					res = res + mu.update(login.getUser().getId());
				}
			
		}

		return res;
	}

	@Override
	public int delete(int id) {							//EFFETTUARE CONTROLLO SU POSIZIONI APERTE

		Copy copy = new Copy();
		List <Handling> handl = new ArrayList <Handling>();
		List <Handling> handc = new ArrayList <Handling>();
		List <Handling> handb = new ArrayList <Handling>();

		int res = -1;

		ManageHandling mhl = new ManageHandling(ManageHandling.LOAN, copy, this.user);

		ManageHandling mhc = new ManageHandling(ManageHandling.CONSULTATION, copy, this.user);

		ManageHandling mhb = new ManageHandling(ManageHandling.BOOKING, copy, this.user);

		handl = mhl.suspendedByUser(id);
		handc = mhc.suspendedByUser(id);
		handb = mhb.suspendedByUser(id);

		//System.out.println("loan " + handl.size());
		//System.out.println("consultation " + handc.size());
		//System.out.println("booking " + handb.size());

		if( handl.size() == 0  && handc.size() == 0) {	//niente in sospeso

			ManageUser mu = new ManageUser(login.getUser());

			String sql = "";

			res =  mu.delete(id);
			//System.out.println("cancello user " + res);

			if(res == 0){	

				sql = "UPDATE login as l SET visibility= false WHERE l.iduser=" + id;
				res = res + req(sql);
				//System.out.println("cancello login " + res);

				if ( res < 0){
					sql = "UPDATE users as u SET visibility= true WHERE u.id = " + id;
					res = res + req(sql);
					//System.out.println("riabilito user " + res);
				}

				//System.out.println("n Booking " + handb.size());
				if (handb.size() > 0){

					for( int i = 0; i < handb.size(); i ++){

						res = mhb.delete(handb.get(i).getId());
						//System.out.println("cancello booking " + res);
					}			
				}
				return res;
			}		
			return res;
		}
		return res;	
	}

	@Override
	public int add() {

		int res = 0;

		List<Login> logins = new ArrayList<Login>();
		List<User> users = new ArrayList<User>();

		logins = this.searchLogin (this.USERNAME, login.getUsername());

		if (logins.size() > 0) {
			return 2;
		}
		else {

			ManageUser mu = new ManageUser(login.getUser());

			users = mu.searchUser(mu.CF, login.getUser().getCf());

			if (users.size() > 0) {
				return 1;
			}
			else {

				res = res + mu.add();

				users = mu.searchUser(mu.CF, login.getUser().getCf());

				login.setUser(users.get(0));

				if (res == 0) {

					login.setUsername (login.getUsername().replace ("'", "''"));
					login.setPassword (login.getPassword().replace ("'", "''"));

					String sql = String.format("INSERT INTO Login (username, password, iduser, visibility) VALUES ('%s','%s', %d, true)",
							login.getUsername(), login.getPassword(), login.getUser().getId());


					res = req(sql);

				}
				else {

					res = -1;
				}
			}
		}
		return res;
	}

	public List<Login> searchLogin(String column, String value) {

		List<Login> logins = new ArrayList <Login> ();

		String sql;

		value = value.replace("'", "''");

		sql = "SELECT u.*, "
			+ "       t.*, "	
			+ "       l.id as loginid, "
			+ "       l.* "
			+ "FROM Users as u "
			+ "INNER JOIN typologies as t " 
			+ "INNER JOIN login as l "
			+ "WHERE u.idtypology = t.id AND l.iduser = u.id AND l.visibility= true AND  " + 
				column + " = '" + value + "'";

				
			List<Map<String, Object>> results = select(sql);

			for(Map<String, Object> result : results) {

				User user = new User();
				Typology typology = new Typology();
				Login login = new Login();

				typology.setId((Integer)result.get("idtypology"));
				typology.setRole((String)result.get("role"));
				typology.setVisibility(true);

				user.setId((Integer)result.get("id"));
				user.setName((String)result.get("name"));
				user.setSurname((String)result.get("surname"));
				user.setBirthDate((Timestamp)result.get("birthdate"));
				user.setCf((String)result.get("cf"));
				user.setBirthPlace((String)result.get("birthplace"));
				user.setDocument((String)result.get("document"));
				user.setNumberDocument((String)result.get("numberdocument"));
				user.setDateDocument((Timestamp)result.get("datedocument"));
				user.setRelease((String)result.get("release"));
				user.setTypology(typology);
				user.setLevel((String)result.get("level"));
				user.setAddress((String)result.get("address"));
				user.setCity((String)result.get("city"));
				user.setTelephone((String)result.get("telephone"));
				user.setEmail((String)result.get("email"));
				user.setEnabled(true);
				user.setVisibility(true);

				login.setId((Integer)result.get("loginid"));
				login.setUsername((String)result.get("username"));
				login.setPassword ((String)result.get("password"));
				login.setUser(user);
				login.setVisibility(true);

				logins.add(login);

			}
		
		return logins;
	}

	public List<Login> getAll () {

		List<Login> logins = new ArrayList <Login> ();

		String sql;

		sql = "SELECT * FROM Users as u, typologies as t, login as l WHERE u.idtypology = t.id AND l.iduser = u.id AND l.visibility= true";
	
		
			List<Map<String, Object>> results = select(sql);

			for(Map<String, Object> result : results) {

				User user = new User();
				DateTime birthDate = new DateTime();
				DateTime dateDocument = new DateTime();
				Typology typology = new Typology();
				Login login = new Login();

				typology.setId((int)result.get("t.id"));
				typology.setRole((String)result.get("role"));
				typology.setVisibility(true);

				birthDate.setDBdateTime((String)result.get("birthDate"));
				dateDocument.setDBdateTime((String)result.get("dateDocument"));

				user.setId((int)result.get("u.id"));
				user.setName((String)result.get("name"));
				user.setSurname((String)result.get("surname"));
				user.setBirthDate((Timestamp)result.get("birthdate"));
				user.setCf((String)result.get("cf"));
				user.setBirthPlace((String)result.get("birthPlace"));
				user.setDocument((String)result.get("document"));
				user.setNumberDocument((String)result.get("numberDocument"));
				user.setDateDocument((Timestamp)result.get("datedocument"));
				user.setRelease((String)result.get("release"));
				user.setTypology(typology);
				user.setLevel((String)result.get("level"));
				user.setAddress((String)result.get("address"));
				user.setCity((String)result.get("city"));
				user.setTelephone((String)result.get("telephone"));
				user.setEmail((String)result.get("email"));
				user.setEnabled(true);
				user.setVisibility(true);

				login.setId((Integer)result.get("l.id"));
				login.setUsername((String)result.get("username"));
				login.setPassword ((String)result.get("password"));
				login.setUser(user);
				login.setVisibility(true);

				logins.add(login);
			}
		
		return logins;
	}
}
