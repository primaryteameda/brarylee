package ita.primaryteam.brarylee.logic;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;

import ita.primaryteam.brarylee.domain.Book;
import ita.primaryteam.brarylee.domain.Copy;
import ita.primaryteam.brarylee.domain.Genre;
import ita.primaryteam.brarylee.domain.Handling;
import ita.primaryteam.brarylee.domain.User;
import ita.primaryteam.brarylee.repository.DBRepository;

// QUESTA CLASSE GESTISCE LE MOVIMENTAZIONI DEL LIBRO
public class ManageBook extends DBRepository implements Manage {

	public final String TITLE = "title";
	public final String AUTHOR = "author";
	public final String ISBN = "isbn";
	public final String GENRE = "g.name";

	private Book book = new Book();

	public ManageBook(Book book) {
		this.book = book;
	}

	// METODO PER MODIFICARE I DATI DI UNO SPECIFICO LIBRO
	@Override
	public int update(int id) {
		book.setVisibility(true);

		int res = 0;

		book.setTitle(book.getTitle().replace("'", "''"));
		book.setAuthor(book.getAuthor().replace("'", "''"));
		book.setPublisher(book.getPublisher().replace("'", "''"));
		book.setEdition(book.getEdition().replace("'", "''"));
		book.setIsbn(book.getIsbn().replace("'", "''"));
		book.setNote(book.getNote().replace("'", "''"));

		String sql = String.format("UPDATE Books b" + 
				"  SET title = '%s'," +
				"	  author = '%s', " +
				"   publisher = '%s', " +
				"   idgenre = %d, " +
				"   year = %d, " +
				"   edition = '%s', " +
				"   isbn = '%s', " +
				"   note = '%s', " +
				"   visibility = %b " +
				"WHERE b.id = %d",
				book.getTitle(), book.getAuthor(), book.getPublisher(), book.getGenre().getId(), book.getYear(), 
				book.getEdition(), book.getIsbn(), book.getNote(), book.isVisible(), id);

		res = req(sql);

		return res;
	}

	//METODO PER CANCELLARE UN LIBRO, CONTROLLA SE CI SONO COPIE IN PRESTITO
	@Override
	public int delete(int id) {  

		int res = 0;
		User user= new User();
		Book book = new Book();

		List<Copy> copies = new ArrayList<Copy>();

		List<Handling> handb = new ArrayList<Handling>();
		List<Handling> handc = new ArrayList<Handling>();
		List<Handling> handl = new ArrayList<Handling>();

		List<Handling> hands = new ArrayList<Handling>();

		book.setId(id);
		ManageCopy mc = new ManageCopy(book);

		copies = mc.getAll(); 

		for(int i=0; i< copies.size(); i++){

			ManageHandling mhb = new ManageHandling(ManageHandling.BOOKING,copies.get(i), user );
			ManageHandling mhc = new ManageHandling(ManageHandling.CONSULTATION, copies.get(i), user);
			ManageHandling mhl = new ManageHandling(ManageHandling.LOAN, copies.get(i), user);


			hands=mhb.suspendedByCopy(copies.get(i).getId()  );

			for(int j=0; j< hands.size(); j++){

				handb.add(hands.get(j)) ;
			}


			hands= mhc.suspendedByCopy(copies.get(i).getId());



			for(int j=0; j< hands.size(); j++){

				handc.add(hands.get(j)) ;
			}
			hands= mhl.suspendedByCopy(copies.get(i).getId());

			for(int j=0; j< hands.size(); j++){

				handl.add(hands.get(j)) ;
			}

		}


		if (handb.size() > 0 || handc.size() > 0 || handl.size() > 0){
			return 1;
		}
		else{


			String sql = String.format("UPDATE Books b SET b.visibility = false WHERE id = %d", id);

			res = req(sql);

			if (res == 0){

				sql = String.format("UPDATE Copies c SET c.visibility = false WHERE idbook = %d", id);

				res = res + req(sql);
			}
			if (res != 0){
				sql = String.format("UPDATE Books b SET b.visibility = true WHERE id = %d", id);

				res = res + req(sql);	
			}
			return res;
		}
	}			

	// METODO CHE RITORNA UNA LISTA DI LIBRI PRESENTI NEL DATABASE
	public List<Book> getAll() {

		List<Book> books = new ArrayList <Book> ();

		String sql = "SELECT * "
				 +   " FROM books as b "
				 +   " INNER JOIN genres as g "
				 +   " WHERE b.visibility=true "
				 +   "       AND b.idgenre = g.id";

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results){

			Book book = new Book();
			Genre genre = new Genre();
			List<Copy> copies = new ArrayList<Copy>();

			book.setId((Integer)result.get("id"));

			ManageCopy mc = new ManageCopy(book);
			copies = mc.getAll();

			genre.setId((Integer)result.get("idgenre"));
			genre.setName((String)result.get("name"));
			genre.setVisibility(true);

			book.setTitle((String)result.get("title"));
			book.setAuthor((String)result.get("author"));
			book.setPublisher((String)result.get("publisher"));
			book.setGenre(genre);
			book.setYear((Integer)result.get("year"));
			book.setEdition((String)result.get("edition"));
			book.setIsbn((String)result.get("isbn"));
			book.setCopies(copies);
			book.setNote((String)result.get("note"));
			book.setVisibility(true);

			books.add(book);
		}


		return books;
	}

	// METODO PER AGGIUNGERE UN LIBRO NEL DATABASE
	@Override
	public int add() {
		book.setVisibility(true);

		int res = 0;

		List<Book> books = new ArrayList<Book>();

		books = this.searchBook(ISBN, book.getIsbn());

		if (books.size() > 0) {
			return 1;
		}
		else {

			book.setTitle(book.getTitle().replace("'", "''"));
			book.setAuthor(book.getAuthor().replace("'", "''"));
			book.setPublisher(book.getPublisher().replace("'", "''"));
			book.setEdition(book.getEdition().replace("'", "''"));
			book.setIsbn(book.getIsbn().replace("'", "''"));
			book.setNote(book.getNote().replace("'", "''"));

			String sql = String.format("INSERT INTO Books(title, author, publisher, idgenre, year, edition, isbn, note, visibility) " + 
					"VALUES ('%s', '%s', '%s', %d, %d, '%s', '%s', '%s', %b)",
					book.getTitle(), book.getAuthor(), book.getPublisher(), book.getGenre().getId(), book.getYear(), book.getEdition(), 
					book.getIsbn(), book.getNote(), book.isVisible());

			res = req(sql);

			return res;
		}
	}

	// METODO PER CERCARE UN LIBRO NEL DATABASE
	public List<Book> searchBook(String column, String value) {

		List<Book> books = new ArrayList <Book> ();

		value.replace("'", "''");

		String sql = "SELECT b.*, "
				 +   "       g.name as genrename "
				 +   "FROM books as b "
				 +   "INNER JOIN genres as g "
				 +   "WHERE b.visibility=true AND b.idgenre = g.id AND " + column + " like '%" + value + "%'";

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results){

			Book book = new Book();
			Genre genre = new Genre();
			List<Copy> copies = new ArrayList<Copy>();

			book.setId((Integer)result.get("id"));

			ManageCopy mc = new ManageCopy(book);
			copies = mc.getAll();

			genre.setId((Integer)result.get("idgenre"));
			genre.setName((String)result.get("genrename"));
			genre.setVisibility(true);

			book.setTitle((String)result.get("title"));
			book.setAuthor((String)result.get("author"));
			book.setPublisher((String)result.get("publisher"));
			book.setGenre(genre);
			book.setYear((Integer)result.get("year"));
			book.setEdition((String)result.get("edition"));
			book.setIsbn((String)result.get("isbn"));
			book.setCopies(copies);
			book.setNote((String)result.get("note"));
			book.setVisibility(true);

			books.add(book);
		}

		return books;
	}
}