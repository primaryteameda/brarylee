package ita.primaryteam.brarylee.logic;

import ita.primaryteam.brarylee.domain.Typology;
import ita.primaryteam.brarylee.repository.DBRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ManageTypology extends DBRepository implements Manage{

	public ManageTypology() {
	}
	
	public List<Typology> gettAll() {
		
		String sql = String.format("SELECT * FROM typologies t WHERE t.visibility = true");
		
		List<Typology> typologies = new ArrayList<Typology>();

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results){
			
			Typology typology = new Typology();
			typology.setId((Integer)result.get("id"));
			typology.setRole((String)result.get("role"));
			typology.setVisibility(true);
			
			typologies.add(typology);
			
		}
		
		return typologies;
	}
	
	@Override
	public int update(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int add() {
		// TODO Auto-generated method stub
		return 0;
	}

}
