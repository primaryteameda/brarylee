package ita.primaryteam.brarylee.logic;

//INTERFACCIA

public interface Manage {
	
	// METODI
	int update(int id);
	
	int delete(int id);

	int add();
}
