package ita.primaryteam.brarylee.logic;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ita.primaryteam.brarylee.domain.Position;
import ita.primaryteam.brarylee.repository.DBRepository;

public class ManagePosition extends DBRepository implements Manage {

	private Position position = new Position();

	public Position newPosition = new Position();


	public ManagePosition(Position position) {

		this.position = position;
	}
	

	public ManagePosition() {
	}

	@Override
	public int update(int id) {

		int res = 0;

		position.setBookcase(position.getBookcase().replace("'", "''"));
		position.setShelf(position.getShelf().replace("'", "''"));

		String sql = String.format("UPDATE positions p SET p.visibility = true, p.bookcase = '%s', p.shelf = '%s' WHERE p.id = %d",
				position.getBookcase(), position.getShelf(), id);

		res = req(sql);

		return res;
	}

	@Override
	public int delete(int id) {

		int res = 0;

		String sql = String.format("UPDATE positions p  SET p.visibility = false WHERE p.id = %d", id);


		res = req(sql);

		if (res == 0){

			sql = String.format("UPDATE Copies c SET c.idposition = " + this.newPosition.getId() + " WHERE idposition = %d", id);

			res = res + req(sql);
		}
		if (res != 0){
			sql = String.format("UPDATE positions p SET p.visibility = true WHERE id = %d", id);

			res = res + req(sql);	
		}
		return 0;
	}

	public List<Position> getAll() {

		String sql = String.format("SELECT * FROM positions p where p.visibility = true ");

		List<Position>positions = new ArrayList<Position>();

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results){

			Position position = new Position();

			position.setId((Integer)result.get("id"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			positions.add(position);
		}	

		return positions;
	}

	public List<Position> searchPosition(String bookCase, String shelf) {

		bookCase.replace("'", "''");
		shelf.replace("'", "''");

		String sql = String.format("SELECT * FROM positions p where p.visibility = true AND p.bookcase= '%s' AND p.shelf= '%s'", bookCase, shelf);


		List<Position>positions = new ArrayList<Position>();

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results){

			Position position = new Position();

			position.setId((Integer)result.get("id"));
			position.setBookcase((String)result.get("bookcase"));
			position.setShelf((String)result.get("shelf"));
			position.setVisibility(true);

			positions.add(position);
		}	

		return positions;
	}

	@Override
	public int add() {

		int res = 0;

		List<Position>positions = new ArrayList<Position>();

		position.setBookcase(position.getBookcase().replace("'", "''"));
		position.setShelf(position.getShelf().replace("'", "''"));

		positions = this.searchPosition(position.getBookcase(), position.getShelf());

		if (positions.size() > 0) {
			return 1;
		}
		else {

			String sql = String.format("INSERT INTO positions(bookcase, shelf, visibility) " + 
					"VALUES ('%s', '%s', %b)", position.getBookcase(), position.getShelf(), position.isVisible());

			res = req(sql);
		}
		return res;
	}
}
