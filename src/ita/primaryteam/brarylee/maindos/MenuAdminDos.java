package ita.primaryteam.brarylee.maindos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import ita.primaryteam.brarylee.domain.Book;
import ita.primaryteam.brarylee.domain.Copy;
import ita.primaryteam.brarylee.domain.DateTime;
import ita.primaryteam.brarylee.domain.Genre;
import ita.primaryteam.brarylee.domain.Handling;
import ita.primaryteam.brarylee.domain.Login;
import ita.primaryteam.brarylee.domain.Position;
import ita.primaryteam.brarylee.domain.Typology;
import ita.primaryteam.brarylee.domain.User;
import ita.primaryteam.brarylee.logic.ManageBook;
import ita.primaryteam.brarylee.logic.ManageCopy;
import ita.primaryteam.brarylee.logic.ManageGenre;
import ita.primaryteam.brarylee.logic.ManageHandling;
import ita.primaryteam.brarylee.logic.ManageLogin;
import ita.primaryteam.brarylee.logic.ManagePosition;
import ita.primaryteam.brarylee.logic.ManageTypology;
import ita.primaryteam.brarylee.logic.ManageUser;
import ita.primaryteam.brarylee.repository.DBRepository;

public class MenuAdminDos {

	DBRepository dbr = new DBRepository();

	List<Book> books = new ArrayList<Book>();
	List<Position> positions = new ArrayList<Position>();
	List<User> users = new ArrayList<User>();
	List<Handling> handlings = new ArrayList<Handling>();
	List<Copy> copies = new ArrayList<Copy>();
	List<Genre> genres = new ArrayList<Genre>();
	List<Typology> typologies = new ArrayList<Typology>();

	int choice;
	String user;

	public void clear() {
		for (int i = 0; i <= 50; i++) {
			System.out.println("");
		}
	}

	public void firstMenu(String user, String message) {

		this.user=user;

		int u = this.user.length();
		if(u < 37) {
			for( int i=0; i<(37-u); i++) {
				this.user = this.user + " ";
			}
		}

		System.out.println("***********************************************");
		System.out.println("* Admin: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*          Biblioteca 'A. Righi'              *");
		System.out.println("*                                             *");
		System.out.println("*           1        Libri                    *");
		System.out.println("*           2        Utenti                   *");
		System.out.println("*           3        Movimentazioni           *");
		System.out.println("*           4        Exit                     *");
		System.out.println("*                                             *");

		this.waiting(message);

	}

	public void waiting(String message) {
		choice = 0;
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		while (true) {

			try {
				choice = Integer.parseInt(inputid.readLine());
			} catch (Exception e) {
				choice = 0;
			}

			switch (choice) {
			case 1:
				this.clear();
				this.printBook("");
				break;
			case 2:
				this.clear();
				this.printUser("");
				break;
			case 3:
				this.clear();
				this.printHandling("");
				break;
			case 4:
				System.out.println("End");
				LoginMain loginMain = new LoginMain();
				this.clear();
				loginMain.printStart("");
				break;
			default:
				this.clear();
				this.firstMenu(user, "Scelta Errata");

			}
		}
	}

	public void printBook(String message) {

		System.out.println("***********************************************");
		System.out.println("* Admin: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*                MENU Libri                   *");
		System.out.println("*                                             *");
		System.out.println("*           1       Lista Libri               *");
		System.out.println("*           2       Cerca                     *");
		System.out.println("*           3       Aggiungi Libro            *");
		System.out.println("*           4       Cancella Libro            *");
		System.out.println("*           5       Modifica Libro            *");
		System.out.println("*           6       Aggiungi Copia            *");
		System.out.println("*           7       Cancella Copia            *");
		System.out.println("*           8       Modifica Copia            *");
		System.out.println("*           9       Menu Precedente           *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.viewAllBook();
			break;
		case 2:
			this.clear();
			this.printSearchBook("");
			break;
		case 3:
			this.clear();
			this.addBook();
			break;
		case 4:
			this.clear();
			this.deleteBook();
			break;
		case 5:
			this.clear();
			this.updateBook();
			break;
		case 6:
			this.clear();
			this.addCopy();
			break;
		case 7:
			this.clear();
			this.deleteCopy();
			break;
		case 8:
			this.clear();
			this.updateCopy();
			break;
		case 9:
			this.clear();
			this.firstMenu(user, "");
			break;
		default:
			this.clear();
			this.printBook("Scelta Errata");
		}
	}

	public void addBook() {
		Book book = new Book();
		Genre genre = new Genre();
		ManageGenre mg = new ManageGenre();

		System.out.println("***********************************************");
		System.out.println("*               INSERISCI LIBRO               *");
		System.out.println("***********************************************");
		System.out.println("                                               ");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.print("Inserisci Titolo: ");

			String titleB = input.readLine();
			book.setTitle(titleB);

			System.out.print("Inserisci Autore: ");

			String authorB = input.readLine();
			book.setAuthor(authorB);

			System.out.print("Inserisci Editore: ");

			String publisherB = input.readLine();
			book.setPublisher(publisherB);
			//visualizzare tutti i generi
			System.out.println("\n");
			Book.pad(20, '*');
			Genre.stampaHeader();
			genres=mg.getAll();
			for (int i = 0; i < genres.size(); i++) {
				genres.get(i).stampaGenre();
			}
			Book.pad(20, '*');
			System.out.println("\n");
			System.out.print("Inserisci Id Genere: ");

			int idgenre;

			try {
				idgenre = Integer.parseInt(input.readLine());
			} catch (Exception e) {
				idgenre = 0;
			}
			genre.setId(idgenre);
			book.setGenre(genre);

			System.out.print("Inserisci Anno: ");

			int year;

			try {
				year = Integer.parseInt(input.readLine());
			} catch (Exception e) {
				year = 0;
			}
			book.setYear(year);

			System.out.print("Inserisci Edizione: ");

			String editionB = input.readLine();
			book.setEdition(editionB);

			System.out.print("Inserisci Isbn: ");

			String isbnB = input.readLine();
			book.setIsbn(isbnB);

			System.out.print("Inserisci Note: ");

			String noteB = input.readLine();
			book.setNote(noteB);

			ManageBook mb = new ManageBook(book);

			int res = 0;

			res = mb.add();
			switch(res) {

			case 0:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*               INSERISCI LIBRO               *");
				System.out.println("***********************************************");
				System.out.println("*            Inserimento Completato           *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case 1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*               INSERISCI LIBRO               *");
				System.out.println("***********************************************");
				System.out.println("*              Libro Esistente                *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case -1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*               INSERISCI LIBRO               *");
				System.out.println("***********************************************");
				System.out.println("*                   Errore                    *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;
			}
			input.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printBook("");

	}

	public void viewAllBook() {

		Book book = new Book();
		ManageBook mb = new ManageBook(book);

		books = mb.getAll();

		this.clear();

		System.out.println("                                                                **************************************************************");
		System.out.println("                                                                *                          LIBRI                             *");
		System.out.println("                                                                *                                                            *");
		System.out.println("                                                                *                                                            *");
		System.out.println("                                                                **************************************************************\n\n");
		//aggiungere il numero di copie
		if(books.size() > 0) {
			Book.pad(280, '*');
			Book.stampaHeader();
			//aggiungere il numero di copie
			for (int i = 0; i < books.size(); i++) {
				//System.out.println(books.get(i).toString());
				books.get(i).stampaLibro();
			}
			Book.pad(280, '*');
			System.out.println("\n");
		}
		else {
			System.out.println("Non ci sono libri");
		}

		System.out.println("*        Premi Un Tasto Per Continuare        *");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printBook("");
	}

	/*public void searchByGenre() {

		Book book = new Book();
		Genre genre = new Genre();
		ManageGenre mg = new ManageGenre(genre);
		ManageBook mb = new ManageBook(book);
		genres = mg.getAll();

		this.clear();

		System.out.println("**************************************************************");
		System.out.println("*                          GENERI                            *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Name               Nr Club      Nationality          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < genres.size(); i++) {
			System.out.println(genres.get(i).toString());
		}

		System.out.println("*                                                            *");
		System.out.println("**************************************************************");
		System.out.println("Scegli Il Genere: ");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value = "";

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		books = mb.searchBook(mb.GENRE, value);

		this.clear();

		System.out.println("**************************************************************");
		System.out.println("*                          LIBRI                             *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Titolo               Autore Editore   Copie   Disponibilit�          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < books.size(); i++) {
			System.out.println(books.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println("*        Premi Un Tasto Per Continuare        *");

		input = new BufferedReader(new InputStreamReader(System.in));

		try {
			input.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printBook("");
	}

	public void searchByAuthor() {

		Book book = new Book();
		ManageBook mb = new ManageBook(book);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		books= mb.searchBook(mb.AUTHOR, value);

		this.clear();

		System.out.println("**************************************************************");
		System.out.println("*                          LIBRI                             *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Titolo               Autore Editore   Copie   Disponibilit�          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < books.size(); i++) {
			System.out.println(books.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println("*        Premi Un Tasto Per Continuare        *");

		input = new BufferedReader(new InputStreamReader(System.in));

		try {
			input.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printBook("");
	}

		public void searchByTitle() {

		Book book = new Book();
		ManageBook mb = new ManageBook(book);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		books= mb.searchBook(mb.TITLE, value);

		this.clear();

		System.out.println("**************************************************************");
		System.out.println("*                          LIBRI                             *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Titolo               Autore Editore   Copie   Disponibilit�          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < books.size(); i++) {
			System.out.println(books.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println("*        Premi Un Tasto Per Continuare        *");

		input = new BufferedReader(new InputStreamReader(System.in));

		try {
			input.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printBook("");
	}

	public void searchByIsbn() {

		Book book = new Book();
		ManageBook mb = new ManageBook(book);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		books= mb.searchBook(mb.ISBN, value);

		this.clear();

		System.out.println("**************************************************************");
		System.out.println("*                          LIBRI                             *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Titolo               Autore Editore   Copie   Disponibilit�          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < books.size(); i++) {
			System.out.println(books.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println("*        Premi Un Tasto Per Continuare        *");

		input = new BufferedReader(new InputStreamReader(System.in));

		try {
			input.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printBook("");
	}*/

	public void updateBook() {

		Genre genre = new Genre();
		Book book = new Book();
		ManageBook mb = new ManageBook(book);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		System.out.print("Inserisci Isbn: ");

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		this.clear();

		books= mb.searchBook(mb.ISBN, value);

		if (books.size() < 1) {

			System.out.println("Isbn Non Trovato");

			this.updateBook();
		}
		else {

			System.out.println("                                                                **************************************************************");
			System.out.println("                                                                *                          LIBRI                             *");
			System.out.println("                                                                *                                                            *");
			System.out.println("                                                                *                                                            *");
			System.out.println("                                                                **************************************************************\n\n");
			Book.pad(280, '*');
			Book.stampaHeader();
			//aggiungere il numero di copie
			for (int i = 0; i < books.size(); i++) {
				//System.out.println(books.get(i).toString());
				books.get(i).stampaLibro();
			}
			Book.pad(280, '*');
			System.out.println("\n");
			System.out.println("*        Scegli Id Libro        *");

			BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
			int id;

			try {
				id = Integer.parseInt(inputid.readLine());
			} catch (Exception e) {
				id = 0;
			}
			book.setId(id);

			System.out.println("***********************************************");
			System.out.println("*               MODIFICA LIBRO                *");
			System.out.println("***********************************************");
			System.out.println("                                               ");
			try {
				System.out.print("Modifica Titolo: ");

				String titleB = input.readLine();
				book.setTitle(titleB);

				System.out.print("Modifica Autore: ");

				String authorB = input.readLine();
				book.setAuthor(authorB);

				System.out.print("Modifica Editore: ");

				String publisherB = input.readLine();
				book.setPublisher(publisherB);
				//visualizzare tutti i generi
				System.out.print("Modifica Id Genere: ");

				int idgenre;

				try {
					idgenre = Integer.parseInt(input.readLine());
				} catch (Exception e) {
					idgenre = 0;
				}
				genre.setId(idgenre);
				book.setGenre(genre);

				System.out.print("Modifica Anno: ");

				int year;

				try {
					year = Integer.parseInt(input.readLine());
				} catch (Exception e) {
					year = 0;
				}
				book.setYear(year);

				System.out.print("Modifica Edizione: ");

				String editionB = input.readLine();
				book.setEdition(editionB);

				System.out.print("Modifica Isbn: ");

				String isbnB = input.readLine();
				book.setIsbn(isbnB);

				System.out.print("Modifica Note: ");

				String noteB = input.readLine();
				book.setNote(noteB);

				int res= 0;

				res = mb.update(id);

				switch(res) {

				case 0:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*               MODIFICA LIBRO                *");
					System.out.println("***********************************************");
					System.out.println("*            Modifica Completata              *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;

				case -1:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*               MODIFICA LIBRO                *");
					System.out.println("***********************************************");
					System.out.println("*                   Errore                    *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;
				}

				input.readLine();

			} catch (Exception e) {
			}

			this.clear();
			this.printBook("");
		}
	}

	public void deleteBook() {

		Book book = new Book();
		ManageBook mb = new ManageBook(book);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		books= mb.searchBook(mb.ISBN, value);

		this.clear();

		System.out.println("                                                                **************************************************************");
		System.out.println("                                                                *                          LIBRI                             *");
		System.out.println("                                                                *                                                            *");
		System.out.println("                                                                *                                                            *");
		System.out.println("                                                                **************************************************************\n\n");
		Book.pad(280, '*');
		Book.stampaHeader();
		//aggiungere il numero di copie
		for (int i = 0; i < books.size(); i++) {
			//System.out.println(books.get(i).toString());
			books.get(i).stampaLibro();
		}
		Book.pad(280, '*');
		System.out.println("\n");
		System.out.println("*        Scegli Id Libro        *");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
		int id;

		try {
			id = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			id = 0;
		}

		int res= 0;

		res = mb.delete(id);

		switch(res) {

		case 0:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*               ELIMINA LIBRO                 *");
			System.out.println("***********************************************");
			System.out.println("*          Cancellazione Completata           *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;

		case -1:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*               ELIMINA LIBRO                 *");
			System.out.println("***********************************************");
			System.out.println("*                   Errore                    *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;
		}

		try {
			inputid.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printBook("");
	}

	public void addCopy() {

		Position position = new Position();
		Copy copy = new Copy();
		Book book = new Book();
		ManageBook mb = new ManageBook(book);
		ManageCopy mc = new ManageCopy(book);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		System.out.print("Inserisci Isbn: ");

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		this.clear();

		books= mb.searchBook(mb.ISBN, value);

		if (books.size() < 1) {

			System.out.println("Isbn Non Trovato");

			this.addCopy();
		}
		else {

			System.out.println("                                                                **************************************************************");
			System.out.println("                                                                *                          LIBRI                             *");
			System.out.println("                                                                *                                                            *");
			System.out.println("                                                                *                                                            *");
			System.out.println("                                                                **************************************************************\n\n");
			Book.pad(280, '*');
			Book.stampaHeader();
			//aggiungere il numero di copie
			for (int i = 0; i < books.size(); i++) {
				//System.out.println(books.get(i).toString());
				books.get(i).stampaLibro();
			}
			Book.pad(280, '*');
			System.out.println("\n");
			System.out.println("*        Scegli Id Libro        *");

			BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
			int id;

			try {
				id = Integer.parseInt(inputid.readLine());
			} catch (Exception e) {
				id = 0;
			}
			book.setId(id);

			System.out.println("***********************************************");
			System.out.println("*               AGGIUNGI COPIA                *");
			System.out.println("***********************************************");
			System.out.println("                                               ");
			try {
				System.out.print("Inserisci Codice: ");

				String codeC = input.readLine();
				copy.setCode(codeC);
				//visualizzare tutte le posizioni
				System.out.print("Inserisci Posizione: ");

				int idposition;

				try {
					idposition = Integer.parseInt(input.readLine());
				} catch (Exception e) {
					idposition = 0;
				}

				position.setId(idposition);

				List<Copy> copies = new ArrayList<Copy>();

				copy.setPosition(position);

				copies.add(copy);

				book.setCopies(copies);

				int res = 0;

				res = mc.add();

				switch(res) {

				case 0:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*               INSERISCI COPIA               *");
					System.out.println("***********************************************");
					System.out.println("*            Inserimento Completato           *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;

				case 1:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*               INSERISCI COPIA               *");
					System.out.println("***********************************************");
					System.out.println("*              Copia Esistente                *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;

				case -1:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*               INSERISCI COPIA               *");
					System.out.println("***********************************************");
					System.out.println("*                   Errore                    *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;
				}

				input.readLine();
			} catch (Exception e) {
			}

			this.clear();
			this.printBook("");
		}
	}
	public void updateCopy() {

		Position position = new Position();
		Copy copy = new Copy();
		Book book = new Book();
		ManagePosition mp = new ManagePosition();
		ManageCopy mc = new ManageCopy(book);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		System.out.print("Inserisci Codice: ");

		try {
			value = input.readLine();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		copy.setCode(value);

		List<Copy> copies = new ArrayList<Copy>();

		copies = mc.searchCopy(mc.CODE, value);

		this.clear();

		copy.setId(copies.get(0).getId());

		System.out.println("***********************************************");
		System.out.println("*               MODIFICA COPIA                *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		try {
			System.out.print("Inserisci Nuovo Codice: ");

			String codeC = input.readLine();
			copy.setCode(codeC);
			//visualizzare tutte le posizioni
			positions=mp.getAll();
			Book.pad(30, '*');
			Position.stampaHeader();
			
			for (int i = 0; i < positions.size(); i++) {
				positions.get(i).stampaPosition();
			}
			Book.pad(30, '*');
			
			System.out.print("\nInserisci Id Posizione: ");

			int idposition;

			try {
				idposition = Integer.parseInt(input.readLine());
			} catch (Exception e) {
				idposition = 0;
			}

			position.setId(idposition);

			copy.setPosition(position);

			List<Copy> copies2 = new ArrayList<Copy>();

			copies2.add(copy);

			book.setCopies(copies2);

			int res = 0;

			res = mc.update(copies2.get(0).getId());

			switch(res) {

			case 0:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*               MODIFICA COPIA                *");
				System.out.println("***********************************************");
				System.out.println("*            Inserimento Completato           *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case -1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*               MODIFICA COPIA                *");
				System.out.println("***********************************************");
				System.out.println("*                   Errore                    *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;
			}

			input.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printBook("");
	}

	public void deleteCopy() {

		Copy copy = new Copy();
		Book book = new Book();
		ManageCopy mc = new ManageCopy(book);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		System.out.print("Inserisci Codice: ");

		try {
			value= input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		copy.setCode(value);

		List<Copy> copies = new ArrayList<Copy>();

		copies = mc.searchCopy(mc.CODE, value);

		this.clear();

		copy.setId(copies.get(0).getId());

		int res = 0;

		res = mc.delete(copies.get(0).getId());

		switch(res) {

		case 0:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*               ELIMINA COPIA                 *");
			System.out.println("***********************************************");
			System.out.println("*           Cancellazione Completato          *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;

		case -1:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*                ELIMINA COPIA                *");
			System.out.println("***********************************************");
			System.out.println("*                   Errore                    *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;
		}
		try {
			input.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.clear();
		this.printBook("");
	}

	public void printSearchBook(String message) {

		System.out.println("***********************************************");
		System.out.println("*                                             *");
		System.out.println("*                Ricerca Libro                *");
		System.out.println("*                                             *");
		System.out.println("*           1       Cerca Titolo              *");
		System.out.println("*           2       Cerca Autore              *");
		System.out.println("*           3       Cerca Genere              *");
		System.out.println("*           4       Menu Precedente           *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.searchBook("titolo");
			break;
		case 2:
			this.clear();
			this.searchBook("autore");
			break;
		case 3:
			this.clear();
			this.searchBook("genere");
			break;
		case 4:
			this.clear();
			this.printBook("");
			break;
		default:
			this.clear();
			this.printSearchBook("Scelta Errata");
		}

	}
	public void searchBook (String column) {

		Book book = new Book();

		ManageBook mb = new ManageBook(book);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*                 CERCA LIBRO                 *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		try {
			System.out.print("Inserisci " + column + ": ");

			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

			String value = input.readLine();

			String col = "";

			switch(column) {

			case "titolo":
				col = mb.TITLE; 
				break;

			case "autore":
				col = mb.AUTHOR;
				break;

			case "genere":
				col = mb.GENRE;
				break;

			}

			books= mb.searchBook(col, value);

			this.clear();

			System.out.println("                                                                **************************************************************");
			System.out.println("                                                                *                          LIBRI                             *");
			System.out.println("                                                                *                                                            *");
			System.out.println("                                                                *                                                            *");
			System.out.println("                                                                **************************************************************\n\n");
			Book.pad(280, '*');
			Book.stampaHeader();
			//aggiungere il numero di copie
			for (int i = 0; i < books.size(); i++) {
				//System.out.println(books.get(i).toString());
				books.get(i).stampaLibro();
			}
			Book.pad(280, '*');
			System.out.println("\n");
			System.out.println("*        Premi Un Tasto Per Continuare        *");

			input = new BufferedReader(new InputStreamReader(System.in));

			try {
				input.readLine();
			} catch (Exception e) {
			}

			this.clear();
			this.printSearchBook("");

		} catch (Exception e) {

		}

	}

	//////////////////////////////////////////// USER////////////////////////////////////////////////////

	public void printUser(String message) {

		System.out.println("***********************************************");
		System.out.println("* Admin: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*                MENU UTENTI                  *");
		System.out.println("*                                             *");
		System.out.println("*           1       Visualizza Utenti         *");
		System.out.println("*           2       Registra Utente           *");
		System.out.println("*           3       Modifica Utente           *");
		System.out.println("*           4       Cancella Utente           *");
		System.out.println("*           5       Cerca Utente              *");
		System.out.println("*           6       Abilita/Disabilita Utente *");
		System.out.println("*           7       Menu Principale           *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.viewAllUser();
			break;
		case 2:
			this.clear();
			this.addUser();
			break;
		case 3:
			this.clear();
			this.updateUser();
			break;
		case 4:
			this.clear();
			this.deleteUser();
			break;
		case 5:
			this.clear();
			this.printSearchUser("");
			break;
		case 6:
			this.clear();
			this.enableUser();
			break;
		case 7:
			this.clear();
			this.firstMenu(user, "");
			break;
		default:
			this.clear();
			this.printUser("Scelta Errata");
		}
	}

	public void addUser() {
		User user = new User();
		Login login = new Login();
		Typology typology = new Typology();
		DateTime birthDate = new DateTime();
		DateTime dateDocument = new DateTime();
		ManageLogin ml = new ManageLogin(login);
		ManageTypology mt = new ManageTypology();
		List<Login> logins = new ArrayList<Login>(); 
		System.out.println("***********************************************");
		System.out.println("*               REGISTRA UTENTE               *");
		System.out.println("***********************************************");
		System.out.println("                                               ");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.print("Inserisci Nome: ");

			String nameU = input.readLine();
			user.setName(nameU);

			System.out.print("Inserisci Cognome: ");

			String surnameU = input.readLine();
			user.setSurname(surnameU);

			System.out.print("Inserisci Data di Nascita (gg/mm/aaaa): ");

			birthDate.setItDate(input.readLine());
			user.setBirthDate(new Timestamp(birthDate.getYear(), birthDate.getMonth(), birthDate.getDay(), 0, 0, 0, 0));

			System.out.print("Inserisci Luogo di Nascita: ");

			String birthPlaceU = input.readLine();
			user.setBirthPlace(birthPlaceU);

			System.out.print("Inserisci Codice Fiscale: ");


			String cfU = input.readLine();

			user.setCf(cfU);

			System.out.print("Inserisci Tipo Documento: ");

			String documentU = input.readLine();
			user.setDocument(documentU);

			System.out.print("Inserisci Nr. Documento: ");

			String numberDocumentU = input.readLine();
			user.setNumberDocument(numberDocumentU);

			System.out.print("Inserisci Data Documento (gg/mm/aaaa): ");

			dateDocument.setItDate(input.readLine());
			user.setDateDocument(new Timestamp(dateDocument.getYear(), dateDocument.getMonth(), dateDocument.getDay(), 0, 0, 0, 0));

			System.out.print("Inserisci Comune di Rilascio: ");

			String releaseU = input.readLine();
			user.setRelease(releaseU);

			
			// stampare elenco delle tipologie
			System.out.println("\n");
			Book.pad(20, '*');
			Typology.stampaHeader();
			typologies= mt.gettAll();
			for (int i = 0; i < typologies.size(); i++) {
				typologies.get(i).stampaTypology();
			}
			Book.pad(20, '*');
			
			System.out.print("\nInserisci ID Tipologia: ");
			int idTypology;

			try {
				idTypology = Integer.parseInt(input.readLine());
			} catch (Exception e) {
				idTypology = 0;
			}

			typology.setId(idTypology);
			user.setTypology(typology);

			System.out.print("Inserisci Livello: ");

			String levelU = input.readLine();
			user.setLevel(levelU);

			System.out.print("Inserisci Indirizzo: ");

			String addressU = input.readLine();
			user.setAddress(addressU);

			System.out.print("Inserisci Citt�: ");

			String cityU = input.readLine();
			user.setCity(cityU);

			System.out.print("Inserisci Telefono: ");

			String telU = input.readLine();
			user.setTelephone(telU);

			System.out.print("Inserisci Email: ");

			String emailU = input.readLine();
			user.setEmail(emailU);

			login.setUser(user);

			System.out.println("Inserisci Username: ");

			String usernameU = input.readLine();

			logins = ml.searchLogin(ml.USERNAME, usernameU);

			while (logins.size() > 0) {

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                  REGISTRATI                 *");
				System.out.println("***********************************************");
				System.out.println("                                               ");
				System.out.println("Username Esistente");
				System.out.println("Inserisci Username: ");

				usernameU = input.readLine();

				logins = ml.searchLogin(ml.USERNAME, usernameU);

			}
			login.setUsername(usernameU);

			System.out.println("Inserisci Password: ");

			String passwordU = input.readLine();


			System.out.println("Conferma Password: ");

			String passwordU2 = input.readLine();

			passwordU.compareTo(passwordU2);

			while (passwordU.compareTo(passwordU2) != 0) {

				System.out.println("Password Diversa");
				System.out.println("Riconferma Password: ");

				passwordU2 = input.readLine();

			} 

			login.setPassword(passwordU);


			ManageLogin ml2 = new ManageLogin(login);

			int res = 0;

			res = ml2.add();
			switch(res) {

			case 0:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*              REGISTRA UTENTE                *");
				System.out.println("***********************************************");
				System.out.println("*            Inserimento Completato           *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case 1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*               REGISTRA UTENTE               *");
				System.out.println("***********************************************");
				System.out.println("*              UTENTE PRESENTE                *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case 2:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*               REGISTRA UTENTE               *");
				System.out.println("***********************************************");
				System.out.println("*             USERNAME PRESENTE               *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case -1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                REGISTRA UTENTE              *");
				System.out.println("***********************************************");
				System.out.println("*                   Errore                    *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;
			}
			input.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printBook("");

	}


	public void viewAllUser() {
		User user = new User();
		ManageUser mu = new ManageUser(user);
		users = mu.getAll();

		this.clear();

		System.out.println("                                                                                                  *********************************************************************");
		System.out.println("                                                                                                  *                              UTENTE                               *");
		System.out.println("                                                                                                  *                                                                   *");
		System.out.println("                                                                                                  *                                                                   *");
		System.out.println("                                                                                                  *********************************************************************\n\n");
		
		Book.pad(275, '*');
		User.stampaHeader();
		for (int i = 0; i < users.size(); i++) {
			users.get(i).stampaUser();
		}
		Book.pad(275, '*');
		System.out.println("\n");
		System.out.println("Premi un tasto per continuare");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));
		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printUser("");
	}

	/*
	public void viewLeague() {

		clubs = mcr.getAll();

		this.clear();

		System.out.println("*********************************************************************");
		System.out.println("*                              CLUBS                                *");
		System.out.println("*                                                                   *");
		System.out.println("* Id   Name               League               President            *");
		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");

		for (int i = 0; i < clubs.size(); i++) {
			System.out.println(clubs.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");
		System.out.println("Choice Club Id: ");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		int idClub;
		try {
			idClub = Integer.parseInt(inputid.readLine());

			leagues = mlr.getLeague(idClub);
		} catch (Exception e) {
			idClub = 0;
		}
		this.clear();


		System.out.println("**************************************************************");
		System.out.println("*                          LEAGUES                           *");
		System.out.println("*                                                            *");
		System.out.println("* Id    Name               Nr Club      Nationality          *");
		System.out.println("*                                                            *");
		System.out.println("**************************************************************");

		for (int i = 0; i < leagues.size(); i++) {
			System.out.println(leagues.get(i).toString());
		}

		System.out.println("*                                                            *");
		System.out.println("**************************************************************");
		System.out.println("Push Any Button For Back");

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printUser("");
	}
}
	 */
	public void updateUser() {

		User user = new User();
		ManageUser mu = new ManageUser(user);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		System.out.print("Inserisci cf: ");

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		this.clear();

		users= mu.searchUser(mu.CF, value);
		user.setId(users.get(0).getId());
		if (users.size() < 1) {

			System.out.println("CF Non Trovato");

			this.updateUser();
		}
		else {

			Login login = new Login(); 
			ManageLogin ml = new ManageLogin(login);

			List<Login> logins = new ArrayList<Login>();

			logins = ml.searchLogin(ml.EMAIL, users.get(0).getEmail());
			//System.out.println("mail -- idl --idu " + users.get(0).getEmail() +"--" + logins.get(0).getId() +"--" + logins.get(0).getUser().getId());

			Typology typology = new Typology();
			DateTime birthDate = new DateTime();
			DateTime dateDocument = new DateTime();

			System.out.println("***********************************************");
			System.out.println("*               MODIFICA UTENTE               *");
			System.out.println("***********************************************");
			System.out.println("                                               ");

			try {
				System.out.print("Inserisci Nome: ");

				String nameU = input.readLine();
				user.setName(nameU);

				System.out.print("Inserisci Cognome: ");

				String surnameU = input.readLine();
				user.setSurname(surnameU);

				System.out.print("Inserisci Data di Nascita (gg/mm/aaaa): ");

				birthDate.setItDate(input.readLine());
				user.setBirthDate(new Timestamp(birthDate.getYear(), birthDate.getMonth(), birthDate.getDay(), 0, 0, 0, 0));

				System.out.print("Inserisci Luogo di Nascita: ");

				String birthPlaceU = input.readLine();
				user.setBirthPlace(birthPlaceU);

				System.out.print("Inserisci Codice Fiscale: ");


				String cfU = input.readLine();

				user.setCf(cfU);

				System.out.print("Inserisci Tipo Documento: ");

				String documentU = input.readLine();
				user.setDocument(documentU);

				System.out.print("Inserisci Nr. Documento: ");

				String numberDocumentU = input.readLine();
				user.setNumberDocument(numberDocumentU);

				System.out.print("Inserisci Data Documento (gg/mm/aaaa): ");

				dateDocument.setItDate(input.readLine());
				user.setDateDocument(new Timestamp(dateDocument.getYear(), dateDocument.getMonth(), dateDocument.getDay(), 0, 0, 0, 0));

				System.out.print("Inserisci Comune di Rilascio: ");

				String releaseU = input.readLine();
				user.setRelease(releaseU);

				System.out.print("Inserisci Tipologia: ");
				// stampare elenco delle tipologie

				int idTypology;

				try {
					idTypology = Integer.parseInt(input.readLine());
				} catch (Exception e) {
					idTypology = 0;
				}

				typology.setId(idTypology);
				user.setTypology(typology);

				System.out.print("Inserisci Livello: ");

				String levelU = input.readLine();
				user.setLevel(levelU);

				System.out.print("Inserisci Indirizzo: ");

				String addressU = input.readLine();
				user.setAddress(addressU);

				System.out.print("Inserisci Citt�: ");

				String cityU = input.readLine();
				user.setCity(cityU);

				System.out.print("Inserisci Telefono: ");

				String telU = input.readLine();
				user.setTelephone(telU);

				System.out.print("Inserisci Email: ");

				String emailU = input.readLine();
				user.setEmail(emailU);

				login.setUser(user);

				System.out.print("Inserisci Username: ");

				String usernameU = input.readLine();
				login.setUsername(usernameU);

				System.out.print("Inserisci Password: ");

				String passwordU = input.readLine();
				login.setPassword(passwordU);


				int res = 0;
				//System.out.println("mail -- idl --idu " + users.get(0).getEmail() +"--" + logins.get(0).getId() +"--" + logins.get(0).getUser().getId());

				res = ml.update(logins.get(0).getId());
				switch(res) {

				case 0:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*              MODIFICA UTENTE                *");
					System.out.println("***********************************************");
					System.out.println("*             Modifica Completata             *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;

				case -1:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*                MODIFICA UTENTE              *");
					System.out.println("***********************************************");
					System.out.println("*                   Errore                    *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;
				}
				input.readLine();
			} catch (Exception e) {
			}

			this.clear();
			this.printUser("");
		}
	}

	public void deleteUser() {

		User user = new User();
		ManageUser mu = new ManageUser(user);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";

		System.out.print("Inserisci cf: ");

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		this.clear();

		users= mu.searchUser(mu.CF, value);
		user.setId(users.get(0).getId());
		user.setEmail(users.get(0).getEmail());

		if (users.size() < 1) {

			System.out.println("CF Non Trovato");

			this.updateUser();
		}
		else {

			Login login = new Login(); 
			ManageLogin ml = new ManageLogin(login);
			List<Login> logins = new ArrayList<Login>();

			logins = ml.searchLogin(ml.EMAIL, user.getEmail());
			login = logins.get(0);
			login.setUser(user);

			int res = 0;
			
			res = ml.delete(login.getId());

			switch(res) {

			case 0:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*               ELIMINA UTENTE                *");
				System.out.println("***********************************************");
				System.out.println("*          Eliminazione Completata            *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case -1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                ELIMINA UTENTE               *");
				System.out.println("***********************************************");
				System.out.println("*                   Errore                    *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;
			}

			try {
				input.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}


			this.clear();
			this.printUser("");
		}
	}

	public void enableUser() {

		User user = new User();
		ManageUser mu = new ManageUser(user);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String value= "";
		String operation= "";

		System.out.print("Inserisci cf: ");

		try {
			value = input.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.print("Inserisci operazione (a: Abilita; d: Disabilita): ");

		try {
			operation = input.readLine();
			if (operation.equals("a")) {

				operation = "abilitato   ";
				user.setEnabled(true);
			}
			else {

				operation = "disabilitato";
				user.setEnabled(false);

			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		this.clear();

		users= mu.searchUser(mu.CF, value);
		user.setId(users.get(0).getId());
		user.setEmail(users.get(0).getEmail());

		if (users.size() < 1) {

			System.out.println("CF Non Trovato");

			this.updateUser();
		}
		else {

			int res = 0;

			res = mu.enabling(user.getId());

			switch(res) {

			case 0:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*             ABILITAZIONE UTENTE             *");
				System.out.println("***********************************************");
				System.out.println("*             Utente " + operation + "        *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case -1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                ELIMINA UTENTE               *");
				System.out.println("***********************************************");
				System.out.println("*                   Errore                    *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;
			}

			try {
				input.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}


			this.clear();
			this.printUser("");
		}
	}


	public void printSearchUser(String message) {

		System.out.println("***********************************************");
		System.out.println("*                                             *");
		System.out.println("*                RICERCA UTENTE               *");
		System.out.println("*                                             *");
		System.out.println("*           1       Cerca Cognome             *");
		System.out.println("*           2       Cerca Citt�               *");
		System.out.println("*           3       Cerca Codice Fiscale      *");
		System.out.println("*           4       Cerca Tipologia           *");
		System.out.println("*           5       Menu Precedente           *");
		System.out.println("*                                             *");
		System.out.println("************* Inserisci scelta  ***************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.searchUser("cognome");
			break;
		case 2:
			this.clear();
			this.searchUser("citt�");
			break;
		case 3:
			this.clear();
			this.searchUser("codice fiscale");
			break;
		case 4:
			this.clear();
			this.searchUser("tipologia");
			break;
		case 5:
			this.clear();
			this.printUser("");
			break;
		default:
			this.clear();
			this.printSearchUser("Scelta Errata");
		}

	}
	
	public void searchUser (String column) {

		User user = new User();

		ManageUser mu = new ManageUser(user);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*                 CERCA UTENTE                *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		try {
			System.out.print("Inserisci " + column + ": ");

			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

			String value = input.readLine();

			String col = "";

			switch(column) {

			case "cognome":
				col = mu.SURNAME; 
				break;

			case "citt�":
				col = mu.CITY;
				break;

			case "codice fiscale":
				col = mu.CF;
				break;

			case "tipologia":
				col = mu.TYPOLOGY;
				break;
			}

			users= mu.searchUser(col, value);

			this.clear();

			System.out.println("                                                                                                  *********************************************************************");
			System.out.println("                                                                                                  *                              UTENTE                               *");
			System.out.println("                                                                                                  *                                                                   *");
			System.out.println("                                                                                                  *                                                                   *");
			System.out.println("                                                                                                  *********************************************************************\n\n");
			
			Book.pad(275, '*');
			User.stampaHeader();
			for (int i = 0; i < users.size(); i++) {
				users.get(i).stampaUser();
			}
			Book.pad(275, '*');
			System.out.println("\n");
			System.out.println("*        Premi Un Tasto Per Continuare        *");

			input = new BufferedReader(new InputStreamReader(System.in));

			try {
				input.readLine();
			} catch (Exception e) {
			}

			this.clear();
			this.printSearchUser("");

		} catch (Exception e) {

		}

}

//////////////////////////////////////////////// MOVIMENTAZIONI /////////////////////////////////////////////////////////////

	public void printHandling(String message) {

		System.out.println("***********************************************");
		System.out.println("* Admin: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*             MENU MOVIMENTAZIONI             *");
		System.out.println("*                                             *");
		System.out.println("*         1       Aggiungi                    *");
		System.out.println("*         2       Restituzione                *");
		System.out.println("*         3       Non Restituiti              *");
		System.out.println("*         4       Prenotazioni                *");
		System.out.println("*         5       Cerca                       *");
		System.out.println("*         6       Menu Precedente             *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.addHandling();
			break;
		case 2:
			this.clear();
			this.restitution();
			break;
		case 3:
			this.clear();
			this.notRestitution();
			break;
		case 4:
			this.clear();
			this.booking();
			break;
		case 5:
			this.clear();
			System.out.println("***********************************************");
			System.out.println("*            CERCA MOVIMENTAZIONE             *");
			System.out.println("***********************************************");
			System.out.println("*                                             *");
			System.out.println("*         1       Prestiti                    *");
			System.out.println("*         2       Consultazioni               *");
			System.out.println("*         3       Prenotazioni                *");
			System.out.println("*                                             *");
			System.out.println("*************** Inserisci Scelta  *************");
			           
			int value=0;
			
			try {

				BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

				value = Integer.parseInt(input.readLine());

				switch(value) {

				case 1:
					this.searchHandling(ManageHandling.LOAN);
					break;

				case 2:
					this.searchHandling(ManageHandling.CONSULTATION);
					break;
				case 3:
					this.searchHandling(ManageHandling.BOOKING);
					break;
				default:
					this.clear();
					this.printHandling("Scelta Errata");
				
				}
				
			
		} catch (Exception e) {
			value= 0;
		}
		case 6:
			this.clear();
			this.firstMenu(user, "");
			break;
		default:
			this.clear();
			this.printHandling("Scelta Errata");
		}
	}
	
	public void searchHandling (String column) {
		
		Copy copy = new Copy();
		User user = new User();

		ManageHandling mh = new ManageHandling(column, copy, user);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*            CERCA MOVIMENTAZIONE             *");
		System.out.println("***********************************************");
		System.out.println("*                                             *");
		System.out.println("*         1       Per Utente                  *");
		System.out.println("*         2       Per Copia                   *");
		System.out.println("*         3       Per Libro                   *");
		System.out.println("*         4       Menu Precedente             *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		
		int value =0;
		String col = "";
		try {

			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			
			value = Integer.parseInt(input.readLine());
			
			switch(value) {

			case 1:
				col = mh.CF_USER; 
				System.out.print("Inserisci CF Utente: ");
				String value2 = input.readLine();
				user.setCf(value2);
				handlings= mh.searchHandling(col, user.getCf());
				break;
			case 2:
				col = mh.CODE;
				System.out.print("Inserisci Codice: ");
				value2 = input.readLine();
				copy.setCode(value2);
				handlings= mh.searchHandling(col, copy.getCode());
				break;
			case 3:
				col = mh.ISBN;
				System.out.print("Inserisci Isbn: ");
				value2 = input.readLine();
				handlings= mh.searchHandling(col, value2);
				break;
			case 4:
				this.clear();
				this.printHandling("");
				break;
			default:
				this.clear();
				this.printHandling("Scelta Errata");
			
			}
		
		
	} catch (Exception e) {
		value= 0;
	}

			

			this.clear();

			System.out.println("                                                        *********************************************************************");
			System.out.println("                                                        *                          MOVIMENTAZIONI                           *");
			System.out.println("                                                        *                                                                   *");
			System.out.println("                                                        *                                                                   *");
			System.out.println("                                                        *********************************************************************\n\n");


			
			Book.pad(200, '*');
			Handling.stampaHeaderAdmin();
			//aggiungere il numero di copie
			for (int i = 0; i < handlings.size(); i++) {
				handlings.get(i).stampaHandlingAdmin();
			}
			Book.pad(200, '*');
			System.out.println("\n");
			System.out.println("*        Premi Un Tasto Per Continuare        *");

			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

			try {
				input.readLine();

			this.clear();
			this.searchHandling("");

		} catch (Exception e) {

		}
}
	
	public void addHandling() {

		Book book = new Book();
		Copy copy = new Copy();
		User user = new User();
		DateTime start = new DateTime();
		DateTime expiry = new DateTime();
		Handling handling = new Handling();

		System.out.println("***********************************************");
		System.out.println("*            AGGIUNGI MOVIMENTAZIONE          *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		System.out.println("*         1       Prestiti                    *");
		System.out.println("*         2       Consultazioni               *");
		System.out.println("*         3       Prenotazioni                *");
		System.out.println("*         4       Menu Precedente             *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");

		int value=0;
		String typeH="";

		try {

			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

			value = Integer.parseInt(input.readLine());

			switch(value) {

			case 1:
				handling.setType(ManageHandling.LOAN);
				typeH = ManageHandling.LOAN;
				expiry.progressDate(90);
				break;

			case 2:
				handling.setType(ManageHandling.CONSULTATION);
				typeH = ManageHandling.CONSULTATION;
				expiry.setCurrentdate();
				expiry.setItTime("22:00:00");
				break;
			case 3:
				handling.setType(ManageHandling.BOOKING);
				typeH = ManageHandling.BOOKING;
				expiry.progressTime(24);
				break;
			case 4:
				this.clear();
				this.printHandling("");
				break;
			default:
				this.clear();
				this.printHandling("Scelta Errata");

			}

		} catch (Exception e) {
			value= 0;
		}

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));


		System.out.print("Inserisci Codice Copia: ");

		String code;

		try {
			code = input.readLine();
		}  catch (Exception e) {
			code="";
		}
		ManageCopy mc = new ManageCopy(book);
		List<Copy> copies = new ArrayList<Copy>();

		copies = mc.searchCopy(mc.CODE, code);
		if (copies.isEmpty()){
			System.out.println("\nCopia inesistente");
		}
		else {
			boolean available=true;
			ManageHandling mhl = new ManageHandling(ManageHandling.LOAN, null, null);
			handlings = mhl.searchHandling(mhl.RESTITUTION, "");
			for (int i = 0; i < handlings.size(); i++) {
				if(code.equals(handlings.get(i).getCopy().getCode())){
					available=false;
				}
			}
			if (available) {
				copy.setId(copies.get(0).getId());
				copy.setCode(code);
				book.setCopies(copies);
				handling.setBook(book);

				System.out.print("Inserisci ID Utente: ");

				int idUser;

				try {
					idUser = Integer.parseInt(input.readLine());
				}  catch (Exception e) {
					idUser=0;
				}

				user.setId(idUser);

				handling.setUser(user);
				handling.setStart(new Timestamp(System.currentTimeMillis()));
				handling.setExpiry(new Timestamp(System.currentTimeMillis()));
				System.out.print(typeH +" " +copy.getCode() +" " + user.getId());
				ManageHandling mh = new ManageHandling(typeH, copy, user);

				int res = 0;

				res = mh.add();
				switch(res) {

				case 0:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*            AGGIUNGI MOVIMENTAZIONE          *");
					System.out.println("***********************************************");
					System.out.println("*            Inserimento Completato           *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;

				case -1:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*            AGGIUNGI MOVIMENTAZIONE          *");
					System.out.println("***********************************************");
					System.out.println("*                   Errore                    *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;
				}
			}
			else {
				System.out.println("\n***********************************************");
				System.out.println("*            AGGIUNGI MOVIMENTAZIONE          *");
				System.out.println("***********************************************");
				System.out.println("*            Copia non disponibile            *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
			}
		}
		try {
			input.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printHandling("");

	}
	
	public void restitution() {
		
		ManageHandling mh = new ManageHandling(ManageHandling.LOAN, null, null);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		
		handlings = mh.searchHandling(mh.RESTITUTION, "");
		
		System.out.println("                                                        *********************************************************************");
		System.out.println("                                                        *                          MOVIMENTAZIONI                           *");
		System.out.println("                                                        *                                                                   *");
		System.out.println("                                                        *                                                                   *");
		System.out.println("                                                        *********************************************************************\n\n");


		
		Book.pad(200, '*');
		Handling.stampaHeaderAdmin();
		//aggiungere il numero di copie
		for (int i = 0; i < handlings.size(); i++) {
			handlings.get(i).stampaHandlingAdmin();
		}
		Book.pad(200, '*');
		System.out.println("\n");
		
		System.out.print("Inserisci id: ");
		
		int res=0;
		
		int id=0;
		try {
			id = Integer.parseInt(input.readLine());
		} catch (IOException e) {
			
			e.printStackTrace();
		} 
		
		res= mh.restitution(id);
		
			switch(res) {

			case 0:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                 RESTITUZIONE                *");
				System.out.println("***********************************************");
				System.out.println("*          Restituzione Completata            *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case -1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                 RESTITUZIONE                *");
				System.out.println("***********************************************");
				System.out.println("*                   Errore                    *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;
			}

			try {
				input.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}


			this.clear();
			this.printHandling("");
		}
	
	public void notRestitution() {
		
		ManageHandling mhl = new ManageHandling(ManageHandling.LOAN, null, null);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		
		handlings = mhl.searchHandling(mhl.RESTITUTION, "");
		
		
		System.out.println("                                                        *********************************************************************");
		System.out.println("                                                        *                          MOVIMENTAZIONI                           *");
		System.out.println("                                                        *                                                                   *");
		System.out.println("                                                        *                                                                   *");
		System.out.println("                                                        *********************************************************************\n\n");


		
		Book.pad(200, '*');
		Handling.stampaHeaderAdmin();
		//aggiungere il numero di copie
		for (int i = 0; i < handlings.size(); i++) {
			handlings.get(i).stampaHandlingAdmin();
		}
		Book.pad(200, '*');
		System.out.println("\n");
		System.out.println("*        Premi Un Tasto Per Continuare        *");

	try {
		input.readLine();
	} catch (IOException e) {
		e.printStackTrace();
	}


	this.clear();
	this.printHandling("");
	}
	
public void booking() {
		
		ManageHandling mhb = new ManageHandling(ManageHandling.BOOKING, null, null);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		
		handlings = mhb.getAllByType();
		
		
		System.out.println("                                                        *********************************************************************");
		System.out.println("                                                        *                          MOVIMENTAZIONI                           *");
		System.out.println("                                                        *                                                                   *");
		System.out.println("                                                        *                                                                   *");
		System.out.println("                                                        *********************************************************************\n\n");


		
		Book.pad(200, '*');
		Handling.stampaHeaderAdmin();
		//aggiungere il numero di copie
		for (int i = 0; i < handlings.size(); i++) {
			handlings.get(i).stampaHandlingAdmin();
		}
		Book.pad(200, '*');
		System.out.println("\n");
		
		System.out.println("*            Scegli id (0: Menu Precedente)                         *");
		
		int value = 0;
		int value2 = 0;
		try {
			value = Integer.parseInt(input.readLine());
		} catch (NumberFormatException e1) {
			
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		switch(value) {

		case 0:
			this.clear();
			this.printHandling("");
			break;
		default:
			this.clear();
			mhb.delete(value);
		System.out.println("*            1: Prestito  2: Consultazione                         *");	
		
			try {
				value2 =Integer.parseInt(input.readLine());
			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Copy copy = new Copy();
			User user = new User();
			int res=0; 
		switch(value2) {

		case 1:

			for(int i =0 ; i< handlings.size(); i++) {
				if (handlings.get(i).getId() == value) {
					copy.setId(handlings.get(i).getBook().getCopies().get(0).getId());
					user.setId(handlings.get(i).getUser().getId());
				}
			}
			
			ManageHandling mhl = new ManageHandling(ManageHandling.LOAN, copy, user);
			res = mhl.add();
			break;
		case 2:

			for(int i =0 ; i< handlings.size(); i++) {
				if (handlings.get(i).getId() == value) {
					copy.setId(handlings.get(i).getBook().getCopies().get(0).getId());
					user.setId(handlings.get(i).getUser().getId());
				}
			}
			
			ManageHandling mhc = new ManageHandling(ManageHandling.CONSULTATION, copy, user);
			res = mhc.add();
			break;
		default:
			this.clear();
			this.printUser("Scelta Errata");
		}
		
		switch(res) {

		case 0:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*                 PRENOTAZIONE                *");
			System.out.println("***********************************************");
			System.out.println("*          Prenotazione Utilizzata            *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;

		case -1:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*                 PRENOTAZIONE                *");
			System.out.println("***********************************************");
			System.out.println("*                   Errore                    *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;
		}

		try {
			input.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}


		this.clear();
		this.printHandling("");
		}	
	}		
}
	