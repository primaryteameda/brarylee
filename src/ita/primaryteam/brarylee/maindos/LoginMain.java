package ita.primaryteam.brarylee.maindos;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import ita.primaryteam.brarylee.domain.Login;
import ita.primaryteam.brarylee.domain.User;
import ita.primaryteam.brarylee.domain.UserLogin;
import ita.primaryteam.brarylee.logic.ManageLogin;

public class LoginMain {

	Login login = new Login();
	ManageLogin ml = new ManageLogin(login);

	User user = new User();
	MenuAdminDos mAdmin = new MenuAdminDos();
	MenuUserDos  mUser = new MenuUserDos();

	public static void main(String[] args) {
		LoginMain menu = new LoginMain();
		menu.printStart("");
	}

	public void printStart(String message) {

		System.out.println("***********************************************");
		System.out.println("*            Biblioteca 'A. Righi'            *");
		System.out.println("*                                             *");
		System.out.println("*                   LOGIN                     *");
		System.out.println("*                                             *");
		System.out.println("*           1        Login                    *");
		System.out.println("*           2        Registrati               *");
		System.out.println("*           3        Esci                     *");
		System.out.println("*                                             *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		int choice = 0;

		MenuAdminDos mad = new MenuAdminDos();
		MenuUserDos mud = new MenuUserDos();

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		while (true) {

			try {
				choice = Integer.parseInt(inputid.readLine());
			} catch (Exception e) {
				choice = 0;
			}

			switch (choice) {
			case 1:
				mad.clear();
				this.printLogin("");
				break;
			case 2:
				mad.clear();
				mud.signIn();
				this.printStart("");
				break;
			case 3:
				System.out.println("End");
				System.exit(1);
			default:
				mad.clear();
				this.printStart("Scelta Errata");
			}
		}
	}

	public void printLogin(String message) {

		System.out.println("***********************************************");
		System.out.println("*                   LOGIN                     *");
		System.out.println("*                                             *");
		System.out.println("*                                             *");
		System.out.println("*                                             *");
		System.out.println("***********************************************");
		System.out.println(message);

		System.out.println("Inserisci Username: ");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		try {
			login.setUsername(input.readLine());
		} catch (Exception e) {

		}

		System.out.println("Inserisci Password: ");

		try {
			login.setPassword(input.readLine());
		} catch (Exception e) {

		}

		user = ml.getLogin();

		switch (user.getLevel()) {
		case "admin":
			mAdmin.clear();
			mAdmin.firstMenu(login.getUsername(),"");
			break;
		case "user":
			mAdmin.clear();
			UserLogin.getInstance().setUserId(user.getId());
			mUser.firstMenu(login.getUsername(),"");
			break;
		case "User Unknown":
			mAdmin.clear();
			this.printStart("Utente non Registrato");
			break;
		case "Password Error":
			mAdmin.clear();
			this.printLogin("Password Errata");
			break;
		default:
			mAdmin.clear();
		}
	}
}

