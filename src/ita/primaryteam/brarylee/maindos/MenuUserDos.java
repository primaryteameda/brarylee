package ita.primaryteam.brarylee.maindos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ita.primaryteam.brarylee.domain.Book;
import ita.primaryteam.brarylee.domain.Copy;
import ita.primaryteam.brarylee.domain.DateTime;
import ita.primaryteam.brarylee.domain.Genre;
import ita.primaryteam.brarylee.domain.Handling;
import ita.primaryteam.brarylee.domain.Login;
import ita.primaryteam.brarylee.domain.Typology;
import ita.primaryteam.brarylee.domain.User;
import ita.primaryteam.brarylee.domain.UserLogin;
import ita.primaryteam.brarylee.logic.ManageBook;
import ita.primaryteam.brarylee.logic.ManageCopy;
import ita.primaryteam.brarylee.logic.ManageHandling;
import ita.primaryteam.brarylee.logic.ManageLogin;
import ita.primaryteam.brarylee.logic.ManageUser;
import ita.primaryteam.brarylee.repository.DBRepository;

public class MenuUserDos {

	DBRepository dbr = new DBRepository();

	List<Book> books = new ArrayList<Book>();
	List<User> users = new ArrayList<User>();
	List<Handling> handlings = new ArrayList<Handling>();
	List<Copy> copies = new ArrayList<Copy>();
	List<Genre> genres = new ArrayList<Genre>();

	int choice;
	String user;

	public void clear() {
		for (int i = 0; i <= 50; i++) {
			System.out.println("");
		}
	}

	public void signIn() {
		User user = new User();
		Login login = new Login();
		Typology typology = new Typology();
		DateTime birthDate = new DateTime();
		DateTime dateDocument = new DateTime();
		MenuAdminDos mad = new MenuAdminDos();
		ManageLogin ml = new ManageLogin(login);
		List<Login> logins = new ArrayList<Login>();

		System.out.println("***********************************************");
		System.out.println("*                  REGISTRATI                 *");
		System.out.println("***********************************************");
		System.out.println("                                               ");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		try {

			System.out.println("Inserisci Username: ");

			String usernameU = input.readLine();

			logins = ml.searchLogin(ml.USERNAME, usernameU);

			while (logins.size() > 0) {

				mad.clear();

				System.out.println("***********************************************");
				System.out.println("*                  REGISTRATI                 *");
				System.out.println("***********************************************");
				System.out.println("                                               ");
				System.out.println("Username Esistente");
				System.out.println("Inserisci Username: ");

				usernameU = input.readLine();

				logins = ml.searchLogin(ml.USERNAME, usernameU);

			}
			login.setUsername(usernameU);

			System.out.println("Inserisci Password: ");

			String passwordU = input.readLine();


			System.out.println("Conferma Password: ");

			String passwordU2 = input.readLine();

			passwordU.compareTo(passwordU2);

			while (passwordU.compareTo(passwordU2) != 0) {

				System.out.println("Password Diversa");
				System.out.println("Riconferma Password: ");

				passwordU2 = input.readLine();

			} 

			login.setPassword(passwordU);

			System.out.print("Inserisci Nome: ");

			String nameU = input.readLine();
			user.setName(nameU);

			System.out.print("Inserisci Cognome: ");

			String surnameU = input.readLine();
			user.setSurname(surnameU);

			System.out.print("Inserisci Data di Nascita (gg/mm/aaaa): ");

			birthDate.setItDate(input.readLine());
			user.setBirthDate(new Timestamp(birthDate.getYear(), birthDate.getMonth(), birthDate.getDay(), 0, 0, 0, 0));

			System.out.print("Inserisci Luogo di Nascita: ");

			String birthPlaceU = input.readLine();
			user.setBirthPlace(birthPlaceU);

			System.out.print("Inserisci Codice Fiscale: ");


			String cfU = input.readLine();

			user.setCf(cfU);

			System.out.print("Inserisci Tipo Documento: ");

			String documentU = input.readLine();
			user.setDocument(documentU);

			System.out.print("Inserisci Nr. Documento: ");

			String numberDocumentU = input.readLine();
			user.setNumberDocument(numberDocumentU);

			System.out.print("Inserisci Data Documento (gg/mm/aaaa): ");

			dateDocument.setItDate(input.readLine());
			user.setDateDocument(new Timestamp(dateDocument.getYear(), dateDocument.getMonth(), dateDocument.getDay(), 0, 0, 0, 0));

			System.out.print("Inserisci Comune di Rilascio: ");

			String releaseU = input.readLine();
			user.setRelease(releaseU);

			typology.setId(1);
			user.setTypology(typology);

			user.setLevel("user");

			System.out.print("Inserisci Indirizzo: ");

			String addressU = input.readLine();
			user.setAddress(addressU);

			System.out.print("Inserisci Citt�: ");

			String cityU = input.readLine();
			user.setCity(cityU);

			System.out.print("Inserisci Telefono: ");

			String telU = input.readLine();
			user.setTelephone(telU);

			System.out.print("Inserisci Email: ");

			String emailU = input.readLine();
			user.setEmail(emailU);

			user.setEnabled(false);

			login.setUser(user);

			ManageLogin ml2 = new ManageLogin(login);

			int res = 0;

			res = ml2.add();
			switch(res) {

			case 0:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                  REGISTRATI                 *");
				System.out.println("***********************************************");
				System.out.println("*           Registrazione Completata          *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case 1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                  REGISTRATI                 *");
				System.out.println("***********************************************");
				System.out.println("*              UTENTE PRESENTE                *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case 2:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                  REGISTRATI                 *");
				System.out.println("***********************************************");
				System.out.println("*             USERNAME PRESENTE               *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;

			case -1:

				this.clear();

				System.out.println("***********************************************");
				System.out.println("*                REGISTRA UTENTE              *");
				System.out.println("***********************************************");
				System.out.println("*                   Errore                    *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
				break;
			}
			input.readLine();
		} catch (Exception e) {
		}

		this.clear();
		//mad.firstMenu("");

	}

	public void firstMenu(String user, String message) {

		this.user=user;

		int u = this.user.length();
		if(u < 37) {
			for( int i=0; i<(37-u); i++) {
				this.user = this.user + " ";
			}
		}

		System.out.println("***********************************************");
		System.out.println("* User: " + this.user + " *");
		System.out.println("*                                             *");
		System.out.println("*          Biblioteca 'A. Righi'              *");
		System.out.println("*                                             *");
		System.out.println("*           1        Libri                    *");
		System.out.println("*           2        Movimentazioni           *");
		System.out.println("*           3        Area personale           *");
		System.out.println("*           4        Exit                     *");
		System.out.println("*                                             *");

		this.waiting(message);

	}

	public void waiting(String message) {
		choice = 0;
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		while (true) {

			try {
				choice = Integer.parseInt(inputid.readLine());
			} catch (Exception e) {
				choice = 0;
			}

			switch (choice) {
			case 1:
				this.clear();
				this.printBook("");
				break;
			case 2:
				this.clear();
				this.printHandling("");
				break;
			case 3:
				this.clear();
				this.printArea("");
				break;
			case 4:
				System.out.println("End");
				LoginMain loginMain = new LoginMain();
				this.clear();
				loginMain.printStart("");
				break;
			default:
				this.clear();
				this.firstMenu(user, "Scelta Errata");

			}
		}
	}
	public void printArea(String message) {

		System.out.println("***********************************************");
		System.out.println("* User: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*                Area personale               *");
		System.out.println("*                                             *");
		System.out.println("*           1       Visualizza dati           *");
		System.out.println("*           2       Elimina account           *");
		System.out.println("*           3       Menu Precedente           *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.viewUser();	
			break;
		case 2:
			this.clear();
			this.deleteUser();
			break;
		case 3:
			this.clear();
			this.firstMenu(user, "");
			break;
		default:
			this.clear();
			this.printBook("Scelta Errata");
		}

	}

	public void viewUser() {

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		User user = new User();

		ManageUser mu = new ManageUser(user);

		users= mu.searchUser(mu.ID, String.valueOf (UserLogin.getInstance().getUserId()));

		this.clear();

		System.out.println("                                                                                                  *********************************************************************");
		System.out.println("                                                                                                  *                              UTENTE                               *");
		System.out.println("                                                                                                  *                                                                   *");
		System.out.println("                                                                                                  *                                                                   *");
		System.out.println("                                                                                                  *********************************************************************\n\n");


		Book.pad(275, '*');
		User.stampaHeader();
		for (int i = 0; i < users.size(); i++) {
			users.get(i).stampaUser();
		}
		Book.pad(275, '*');
		System.out.println("\n");
		
		/*for (int i = 0; i < users.size(); i++) {
			System.out.println(users.get(i).toString());
		}

		System.out.println("*                                                                   *");
		System.out.println("*********************************************************************");*/
		System.out.println("*        Premi Un Tasto Per Continuare        *");

		input = new BufferedReader(new InputStreamReader(System.in));

		try {
			input.readLine();
		} catch (Exception e) {
		}

		this.clear();
		this.printArea("");

	}

	public void deleteUser() {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		User user = new User();
		ManageUser mu = new ManageUser(user);
		users= mu.searchUser(mu.ID, String.valueOf (UserLogin.getInstance().getUserId()));
		user.setId(users.get(0).getId());
		user.setEmail(users.get(0).getEmail());
		Login login = new Login(); 
		ManageLogin ml = new ManageLogin(login);
		List<Login> logins = new ArrayList<Login>();

		logins = ml.searchLogin(ml.EMAIL, user.getEmail());
		login = logins.get(0);
		login.setUser(user);

		int res = 0;
		res = ml.delete(login.getId());

		switch(res) {

		case 0:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*               ELIMINA UTENTE                *");
			System.out.println("***********************************************");
			System.out.println("*          Eliminazione Completata            *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;

		case -1:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*                ELIMINA UTENTE               *");
			System.out.println("***********************************************");
			System.out.println("*                   Errore                    *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;
		}

		try {
			input.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("End");
		System.exit(1);
	}

	public void printBook(String message) {

		System.out.println("***********************************************");
		System.out.println("* User: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*                MENU Libri                   *");
		System.out.println("*                                             *");
		System.out.println("*           1       Lista Libri               *");
		System.out.println("*           2       Cerca                     *");
		System.out.println("*           3       Menu Precedente           *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.viewAllBook();
			break;
		case 2:
			this.clear();
			this.printSearchBook("");
			break;
		case 3:
			this.clear();
			this.firstMenu(user, "");
			break;
		default:
			this.clear();
			this.printBook("Scelta Errata");
		}
	}

	public void viewAllBook() {

		Book book = new Book();
		ManageBook mb = new ManageBook(book);

		books = mb.getAll();

		this.clear();

		System.out.println("                                                                **************************************************************");
		System.out.println("                                                                *                          LIBRI                             *");
		System.out.println("                                                                *                                                            *");
		System.out.println("                                                                *                                                            *");
		System.out.println("                                                                **************************************************************\n\n");
		Book.pad(280, '*');
		Book.stampaHeader();
		//aggiungere il numero di copie
		for (int i = 0; i < books.size(); i++) {
			//System.out.println(books.get(i).toString());
			books.get(i).stampaLibro();
		}
		Book.pad(280, '*');
		System.out.println("\n");
		System.out.println("*        Premi Un Tasto Per Continuare        *");

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			inputid.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printBook("");
	}


	public void printSearchBook(String message) {

		System.out.println("***********************************************");
		System.out.println("*                                             *");
		System.out.println("*                Ricerca Libro                *");
		System.out.println("*                                             *");
		System.out.println("*           1       Cerca Titolo              *");
		System.out.println("*           2       Cerca Autore              *");
		System.out.println("*           3       Cerca Genere              *");
		System.out.println("*           4       Menu Precedente           *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.searchBook("titolo");
			break;
		case 2:
			this.clear();
			this.searchBook("autore");
			break;
		case 3:
			this.clear();
			this.searchBook("genere");
			break;
		case 4:
			this.clear();
			this.printBook("");
			break;
		default:
			this.clear();
			this.printSearchBook("Scelta Errata");
		}

	}

	public void searchBook (String column) {

		Book book = new Book();

		ManageBook mb = new ManageBook(book);

		this.clear();

		System.out.println("***********************************************");
		System.out.println("*                 CERCA LIBRO                 *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		try {
			System.out.print("Inserisci " + column + ": ");

			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

			String value = input.readLine();

			String col = "";

			switch(column) {

			case "titolo":
				col = mb.TITLE; 
				break;

			case "autore":
				col = mb.AUTHOR;
				break;

			case "genere":
				col = mb.GENRE;
				break;

			}

			books= mb.searchBook(col, value);

			this.clear();

			System.out.println("                                                                **************************************************************");
			System.out.println("                                                                *                          LIBRI                             *");
			System.out.println("                                                                *                                                            *");
			System.out.println("                                                                *                                                            *");
			System.out.println("                                                                **************************************************************\n\n");
			Book.pad(280, '*');
			Book.stampaHeader();
			//aggiungere il numero di copie
			for (int i = 0; i < books.size(); i++) {
				//System.out.println(books.get(i).toString());
				books.get(i).stampaLibro();
			}
			Book.pad(280, '*');
			System.out.println("\n");
			System.out.println("*        Premi Un Tasto Per Continuare        *");

			input = new BufferedReader(new InputStreamReader(System.in));

			try {
				input.readLine();
			} catch (Exception e) {
			}

			this.clear();
			this.printSearchBook("");

		} catch (Exception e) {

		}
	}

	////////////////////////////////////////////////MOVIMENTAZIONI /////////////////////////////////////////////////////////////

	public void printHandling(String message) {

		System.out.println("***********************************************");
		System.out.println("* User: " + this.user + "*");
		System.out.println("*                                             *");
		System.out.println("*             MENU MOVIMENTAZIONI             *");
		System.out.println("*                                             *");
		System.out.println("*         1       Aggiungi                    *");
		System.out.println("*         2       Restituzione                *");
		System.out.println("*         3       Non Restituiti              *");
		System.out.println("*         4       Cerca                       *");
		System.out.println("*         5       Menu Precedente             *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");
		System.out.println(message);

		BufferedReader inputid = new BufferedReader(new InputStreamReader(System.in));

		try {
			choice = Integer.parseInt(inputid.readLine());
		} catch (Exception e) {
			choice = 0;
		}

		switch (choice) {
		case 1:
			this.clear();
			this.addHandling();
			break;
		case 2:
			this.clear();
			this.restitution();
			break;
		case 3:
			this.clear();
			this.notRestitution();
			break;
		case 4:
			this.clear();
			System.out.println("***********************************************");
			System.out.println("*            CERCA MOVIMENTAZIONE             *");
			System.out.println("***********************************************");
			System.out.println("*                                             *");
			System.out.println("*         1       Prestiti                    *");
			System.out.println("*         2       Consultazioni               *");
			System.out.println("*         3       Prenotazioni                *");
			System.out.println("*                                             *");
			System.out.println("*************** Inserisci Scelta  *************");

			int value=0;

			try {

				BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

				value = Integer.parseInt(input.readLine());

				switch(value) {

				case 1:
					this.searchHandling(ManageHandling.LOAN);
					break;

				case 2:
					this.searchHandling(ManageHandling.CONSULTATION);
					break;
				case 3:
					this.searchHandling(ManageHandling.BOOKING);
					break;
				default:
					this.clear();
					this.printHandling("Scelta Errata");

				}	

			} catch (Exception e) {
				value= 0;
			}

			break;
		case 5:
			this.clear();
			this.firstMenu(user, "");
			break;
		default:
			this.clear();
			this.printHandling("Scelta Errata");
		}
	}

	public void notRestitution() {

		ManageHandling mhl = new ManageHandling(ManageHandling.LOAN, null, null);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		handlings = mhl.searchNotRestitution(mhl.ID_USER, String.valueOf (UserLogin.getInstance().getUserId()));


		System.out.println("                                            *********************************************************************");
		System.out.println("                                            *                          MOVIMENTAZIONI                           *");
		System.out.println("                                            *                                                                   *");
		System.out.println("                                            *                                                                   *");
		System.out.println("                                            *********************************************************************\n\n");


		
		Book.pad(150, '*');
		Handling.stampaHeader();
		//aggiungere il numero di copie
		for (int i = 0; i < handlings.size(); i++) {
			handlings.get(i).stampaHandlingUser();
		}
		Book.pad(150, '*');
		System.out.println("\n");
		System.out.println("*        Premi Un Tasto Per Continuare        *");

		try {
			input.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.clear();
		this.printHandling("");
	}

	public void addHandling() {

		Book book = new Book();
		Copy copy = new Copy();
		User user = new User();
		DateTime start = new DateTime();
		DateTime expiry = new DateTime();
		Handling handling = new Handling();

		System.out.println("***********************************************");
		System.out.println("*            AGGIUNGI MOVIMENTAZIONE          *");
		System.out.println("***********************************************");
		System.out.println("                                               ");
		System.out.println("*         1       Prestiti                    *");
		System.out.println("*         2       Consultazioni               *");
		System.out.println("*         3       Prenotazioni                *");
		System.out.println("*         4       Menu Precedente             *");
		System.out.println("*                                             *");
		System.out.println("*************** Inserisci Scelta  *************");

		int value=0;
		String typeH="";

		try {

			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

			value = Integer.parseInt(input.readLine());

			switch(value) {

			case 1:
				handling.setType(ManageHandling.LOAN);
				typeH = ManageHandling.LOAN;
				expiry.progressDate(90);
				break;

			case 2:
				handling.setType(ManageHandling.CONSULTATION);
				typeH = ManageHandling.CONSULTATION;
				expiry.setCurrentdate();
				expiry.setItTime("22:00:00");
				break;
			case 3:
				handling.setType(ManageHandling.BOOKING);
				typeH = ManageHandling.BOOKING;
				expiry.progressTime(24);
				break;
			case 4:
				this.clear();
				this.printHandling("");
				break;
			default:
				this.clear();
				this.printHandling("Scelta Errata");

			}

		} catch (Exception e) {
			value= 0;
		}

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));


		System.out.print("Inserisci Codice Copia: ");

		String code;

		try {
			code = input.readLine();
		}  catch (Exception e) {
			code="";
		}
		ManageCopy mc = new ManageCopy(book);
		List<Copy> copies = new ArrayList<Copy>();

		copies = mc.searchCopy(mc.CODE, code);
		if (copies.isEmpty()){
			System.out.println("\nCopia inesistente");
		}
		else {
			boolean available=true;
			ManageHandling mhl = new ManageHandling(ManageHandling.LOAN, null, null);
			handlings = mhl.searchHandling(mhl.RESTITUTION, "");
			for (int i = 0; i < handlings.size(); i++) {
				if(code.equals(handlings.get(i).getCopy().getCode())){
					available=false;
				}
			}
			if (available) {
				copy.setId(copies.get(0).getId());
				copy.setCode(code);
				book.setCopies(copies);
				handling.setBook(book);

				int idUser;

				try {
					idUser = UserLogin.getInstance().getUserId();
				}  catch (Exception e) {
					idUser=0;
				}

				user.setId(idUser);

				handling.setUser(user);
				handling.setStart(new Timestamp(System.currentTimeMillis()));
				handling.setExpiry(new Timestamp(System.currentTimeMillis()));

				System.out.print(typeH +" " +copy.getCode() +" " + user.getId());
				ManageHandling mh = new ManageHandling(typeH, copy, user);

				int res = 0;

				res = mh.add();

				switch(res) {

				case 0:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*            AGGIUNGI MOVIMENTAZIONE          *");
					System.out.println("***********************************************");
					System.out.println("*            Inserimento Completato           *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;

				case -1:

					this.clear();

					System.out.println("***********************************************");
					System.out.println("*            AGGIUNGI MOVIMENTAZIONE          *");
					System.out.println("***********************************************");
					System.out.println("*                   Errore                    *");
					System.out.println("*        Premi Un Tasto Per Continuare        *");
					System.out.println("***********************************************");
					break;
				}
			}
			else {
				System.out.println("\n***********************************************");
				System.out.println("*            AGGIUNGI MOVIMENTAZIONE          *");
				System.out.println("***********************************************");
				System.out.println("*            Copia non disponibile            *");
				System.out.println("*        Premi Un Tasto Per Continuare        *");
				System.out.println("***********************************************");
			}
		}
		try {
			input.readLine();
		} catch (Exception e) {
		}
		this.clear();
		this.printHandling("");

	}


	public void restitution() {

		ManageHandling mh = new ManageHandling(ManageHandling.LOAN, null, null);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		
		handlings = mh.searchNotRestitution(mh.ID_USER, String.valueOf (UserLogin.getInstance().getUserId()));


		System.out.println("                                            *********************************************************************");
		System.out.println("                                            *                          MOVIMENTAZIONI                           *");
		System.out.println("                                            *                                                                   *");
		System.out.println("                                            *                                                                   *");
		System.out.println("                                            *********************************************************************\n\n");


		
		Book.pad(150, '*');
		Handling.stampaHeader();
		//aggiungere il numero di copie
		for (int i = 0; i < handlings.size(); i++) {
			handlings.get(i).stampaHandlingUser();
		}
		Book.pad(150, '*');
		System.out.println("\n");		

		System.out.print("Inserisci id: ");

		int res=0;

		int id=0;
		try {
			id = Integer.parseInt(input.readLine());
		} catch (Exception e) {

			e.printStackTrace();
		} 

		res= mh.restitution(id);

		switch(res) {

		case 0:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*                 RESTITUZIONE                *");
			System.out.println("***********************************************");
			System.out.println("*          Restituzione Completata            *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;

		case -1:

			this.clear();

			System.out.println("***********************************************");
			System.out.println("*                 RESTITUZIONE                *");
			System.out.println("***********************************************");
			System.out.println("*                   Errore                    *");
			System.out.println("*        Premi Un Tasto Per Continuare        *");
			System.out.println("***********************************************");
			break;
		}

		try {
			input.readLine();
		} catch (Exception e) {
			e.printStackTrace();
		}


		this.clear();
		this.printHandling("");
	}


	public void searchHandling (String column) {

		Copy copy = new Copy();
		User user = new User();

		ManageHandling mh = new ManageHandling(column, copy, user);

		String col = mh.ID_USER; 
		int value2 = UserLogin.getInstance().getUserId();
		user.setId(value2);
		handlings= mh.searchHandling(col, String.valueOf (user.getId()));

		this.clear();

		System.out.println("                                            *********************************************************************");
		System.out.println("                                            *                          MOVIMENTAZIONI                           *");
		System.out.println("                                            *                                                                   *");
		System.out.println("                                            *                                                                   *");
		System.out.println("                                            *********************************************************************\n\n");


		
		Book.pad(150, '*');
		Handling.stampaHeader();
		//aggiungere il numero di copie
		for (int i = 0; i < handlings.size(); i++) {
			handlings.get(i).stampaHandlingUser();
		}
		Book.pad(150, '*');
		System.out.println("\n");
		System.out.println("*        Premi Un Tasto Per Continuare        *");

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		try {
			input.readLine();

			this.clear();
			this.printHandling("");

		} catch (Exception e) {

		}
	}

}
