package ita.primaryteam.brarylee.repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBRepository{

	private static String URL;
	private static final String USER = "root";
	private static final String PASSWORD = "test";
	
	public  DBRepository() {

		String dbAddress  = "";

		try(BufferedReader bufferedReader = new BufferedReader(new FileReader("dbaddress.sys"))) {

			dbAddress  = bufferedReader.readLine();

		}catch (Exception e) {
			System.out.println(" file Open Error ");
			e.printStackTrace();
		}
		
		URL = dbAddress;
	}

	public int req(String sql) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD))  {
			Statement statement = connection.createStatement();
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
			return -1;
		}
		return 0;
	}
	
	public List<Map<String, Object>> select(String sql) {
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnCount = rsmd.getColumnCount();

			while(resultSet.next()) {
				Map<String, Object> result = new HashMap<>();

				for (int i = 1; i <= columnCount; i++) {
					String columnName = rsmd.getColumnLabel(i);
					result.put(columnName, resultSet.getObject(columnName));
				}
				results.add(result);
			}
		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}
		return results;
	}
}